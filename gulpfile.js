var gulp = require('gulp');
var wrapper = require('gulp-wrapper');
var concat = require('gulp-concat');
var replace =  require('gulp-replace');
var fs = require('fs');
var prefix = require('gulp-autoprefixer');

gulp.task('compileHtml', [], function(){
	return gulp.src('./src/templates/**')
		.pipe(wrapper({
			header: '<script type="text/html" id="${filename}">\n',
			footer: '\n</script>'
		}))
		.pipe(concat('templates.html'))
		.pipe(gulp.dest('./build'))
		.pipe(gulp.src('./src/html/index.html'))
		// inject templates into base html
	  	.pipe(replace('<!-- replace:templates-->', fs.readFileSync('build/templates.html', 'utf8')))
	  	.pipe(gulp.dest('./'));
});

gulp.task('compileJs', function(){
	return gulp.src('./src/js/**')
	.pipe(concat('all.js'))
	.pipe(gulp.dest('./'));
});

gulp.task('compileCss', function(){
	return gulp.src('./src/css/*')
	.pipe(prefix({cascase: true}))
	.pipe(concat('all.css'))
	.pipe(gulp.dest('./'));
});

gulp.task('watch', function(){
	gulp.watch('./src/css/**', ['compileCss']);
	gulp.watch('./src/js/**', ['compileJs']);
	gulp.watch('./src/html/**, ./src/templates**', ['compilehtml']);
	return null;
});


gulp.task('default', ['compileHtml', 'compileJs', 'compileCss']);