window.robo = window.robo || {};
window.robo.settings = {
	tilesize: 50,
	tileGap: 0
};

/****************************
		UTILITIES
****************************/

robo.util = robo.util || {};

robo.util.loadMapJson = function(definition){
	var arr = [];
	
	var w = definition.width;
	var h = definition.height;
	var defaultTile = definition.default;

	for(var i = 0; i < w*h; i++){
		var data = definition.tiles[i];
		if(_.isString(data) || !data){
			var name = data || defaultTile;	
			data = {
				name: name,
				settings: {}
			};
		}
		data.definition = robo.util.tileDfns[data.name];
		arr.push(data); 
	}

	var board = new robo.board(w, h);
	board.fillBoard(arr);
	return board;
}

/****************************
		GENERAL CODE
****************************/
var loader;

var sampleMap = {
	width: 10,
	height: 10,
	default: 'vacant',
	tiles:[
		null, null, null, null, 'solid', null, null, null, null, null,
		null, null, null, null, null, null, null, null, null, null,
		null, null, null, {name: 'door', settings:{open:true}}, {name: 'door', settings:{open:false}}, null, null, null, null, null,
		null, null, null, null, null, null, null, 'solid', null, null,
		null, null, null, null, null, null, null, null, null, null,
		null, null, null, null, null, 'solid', null, null, null, null,
		null, null, 'solid', null, null, null, null, null, null, null,
		null, null, null, null, null, null, null, null, null, 'solid',
		null, null, null, null, null, null, null, null, null, null,
		{"name":"avatarStartN","settings":{}}, null, null, null, {name:'laser', settings:{active:true}}, null, null, 'solid', null, null
	]
};

var sampleCards = 	[
		"rotate90CW",
		"rotate90CW",
		"rotate90CCW",
		"rotate90CCW",
		"rotate90CW",
		"rotate90CCW"
	];

var levelJson = {
	map: sampleMap,
	cards: sampleCards
};

function handleComplete(){

	engine.library.parseAssets(loader);

	robo.util.sprites = {};

	robo.util.sprites.crono = engine.library.get("sprite", "crono");

	robo.util.sprites.tile = engine.library.get("sprite", "tile");
	robo.util.sprites.tileSolid = engine.library.get("sprite", "tile-solid");
	robo.util.sprites.tilePit = engine.library.get("sprite", "tile-pit");
	robo.util.sprites.tileStart = engine.library.get("sprite", "tile-start");
	robo.util.sprites.tileEnd = engine.library.get("sprite", "tile-end");

	robo.util.icons = {};
	robo.util.icons.rotate = engine.library.get("icon", "rotate");
	robo.util.icons.rotateCCW = engine.library.get("icon", "rotateCCW");
	robo.util.icons.tile = engine.library.get("icon", "tile-icon");
	robo.util.icons.tileSolid = engine.library.get("icon", "tile-solid-icon");
	robo.util.icons.tilePit = engine.library.get("icon", "tile-pit-icon");
	robo.util.icons.tileStart = engine.library.get("icon", "tile-start-icon");
	robo.util.icons.tileEnd = engine.library.get("icon", "tile-end-icon");

	robo.util.cardDfns.rotate90CW = new robo.cardDfn(
		'rotate90CW',
		function(avatar, tile, board){
			avatar.turnCW(1);
		},
		robo.util.icons.rotate,
		{r:200, g: 100, b:0}
	);

	robo.util.cardDfns.rotate90CCW = new robo.cardDfn(
		'rotate90CCW',
		function(avatar, tile, board){
			
			avatar.turnCW(-1);
		},
		robo.util.icons.rotateCCW,
		{r:200, g: 100, b:0}
	);

	// robo.util.tileDfns.vacant.buildResources(engine.library);
	
	_.each(robo.util.tileDfns, function(item, index){
		item.buildResources(engine.library);
	});

	engine.loadLevel(levelJson);

	//Enable touch/mouse events
	createjs.Touch.enable(window.stage);
	window.stage.enableMouseOver(10);
	window.stage.mouseMoveOutside = true; // keep tracking the mouse even when it leaves the canvas

	createjs.Ticker.setFPS(20);
	createjs.Ticker.addEventListener('tick', function(event){
		engine.update(event);
	});

}

$(function(){

	window.stage = new createjs.Stage($('.easel')[0]);
	
	var regions = {
		buttonbar: $(".button-bar")[0],
		edit: $(".edit")[0],
		view: $(".view")[0]
	};

	window.engine = (new robo.engine()).init(window.stage, regions, []);

	setTileDfns();

	//Load assets
	var manifest = getManifest();

	loader = engine.library.getLoader();
	loader.addEventListener("complete", handleComplete);
	loader.addEventListener("progress", function(event){
		$('.progress-bar').css('width', (event.progress * 100).toFixed(2) + "%");
		$('.progress-text').html((event.progress * 100).toFixed(2) + "%");
	});
//	loader.loadManifest(manifest);

	engine.library.addResources(manifest);
	engine.library.load();
});


var setTileDfns = function(){
	robo.util.tileDfns = {};

	robo.util.tileDfns.vacant = new robo.tileDfn.vacant();

	robo.util.tileDfns.solid = new robo.tileDfn.solid();

	robo.util.tileDfns.pit = new robo.tileDfn.pit();

	robo.util.tileDfns.avatarStartN = new robo.tileDfn.start("N");
	robo.util.tileDfns.avatarEnd = new robo.tileDfn.end();

	robo.util.tileDfns.door = new robo.tileDfn.door();
	// robo.util.tileDfns.laser = new robo.tileDfn.laser();
	robo.util.tileDfns.switch = new robo.tileDfn.switch();
	robo.util.tileDfns.laser = new robo.tileDfn.laser();
};

var getManifest = function(){
	return [
//Avatar stuff	
		{src:"img/crono.png", id:"sprite.crono", options:{
			"frames":{"height": 72, "count": 16, "width":48},
			"animations": {
				"walkDown": 	[0,3,"walkDown", 1.0],
				"walkLeft": 	[4,7,"walkLeft", 1.0],
				"walkRight": 	[8,11,"walkRight", 1.0],
				"walkUp": 		[12,15,"walkUp", 1.0]
			}			
		}},
// Tile Sprites
		{src:"img/tile.png", id:"sprite.tile", options: {
			frames: { count: 4},
			animations: {idle: [0,3,"idle", 0.25]}
		}},
		{src:"img/tile-solid.png", id:"sprite.tile-solid", options: {
			frames: { count: 4},
			animations: {idle: [0,3,"idle", 0.25]}
		}},
		{src:"img/tile-pit.png", id:"sprite.tile-pit"},
		{src:"img/tile-start.png", id:"sprite.tile-start"},
		{src:"img/tile-end.png", id:"sprite.tile-end"},
		{src:"img/tile-door.png", id:"sprite.tile-door", options:{
			frames: { count: 10},
			animations: {
				'open': [9,9, 'open', 1.0],
				'closed': [0,0,'closed', 1.0],
				'opening':[0,9, 'open', 1.0],
				'closing':{
					'frames': [8,7,6,5,4,3,2,1],
					'next': 'closed',
					'speed': 1
				}
			}
		}},		
		{src:"img/tile-laser.png", id:"sprite.tile-laser", options:{
			frames: { count: 3},
			animations: {
				'deactive': [0,0,'deactive', 1.0],
				'activating': [0,2,'active', 1.0],
				'active': [2,2,'active', 1.0],
				'deactivating':{
					'frames': [2,1,0],
					'next': 'deactive',
					'speed': 1
				}
			}
		}},
		{ src: 'img/tile-switch.png', id: 'sprite.tile-switch', options:{
			frames: { count: 2},
			animations: {
				idle: { frames: [0], next:  'idle', speed: 1},
				on: { frames: [0], next: 'on', speed: 1},
				off: { frames: [1], next: 'off',speed: 1}
			}
		}},
// Tile Icons 
		{src:"img/tile-icon.png", id:"icon.tile-icon"},
		{src:"img/tile-solid-icon.png", id:"icon.tile-solid-icon"},
		{src:"img/tile-pit-icon.png", id:"icon.tile-pit-icon"},
		{src:"img/tile-start-icon.png", id:"icon.tile-start-icon"},
		{src:"img/tile-end-icon.png", id:"icon.tile-end-icon"},
		{src:"img/tile-door-icon.png", id:"icon.tile-door-icon"},
		{src:"img/tile-laser-icon.png", id:"icon.tile-laser-icon"},
		{src:"img/tile-switch-icon.png", id:"icon.tile-switch-icon"},
//Tile Inspector
		{src:"img/tile-inspect-icon.png", id:"icon.tile-inspect-icon"},
//Card Icons
		{src:"img/rotate.png", id:"icon.rotate"},
		{src:"img/rotateCCW.png", id:"icon.rotateCCW"}
	];
};