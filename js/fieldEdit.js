/*
tiles have a settings array, of elements like: {id: blah, type: blah, value: blah};

*/

robo = robo || {};

robo.gui = function(){
	this.regions = {};
};

robo.gui.prototype.focus = function(entity, region){
	var reg = this.regions[region];
	var settings = entity.getSettings ? entity.getSettings() : null;
	if(reg && settings){
		var fieldSet = (new robo.gui.fieldSet()).init().readSettings(settings).setCallback(entity.applySettings.bind(entity));
		fieldSet.renderInto(reg, false);
	}
}

robo.gui.prototype.init = function(regions){
	this.addRegions(regions);
	return this;
};

robo.gui.prototype.addRegions = function(regions){
	var self = this;
	_.each(regions, function(region, name){
		self.addRegion(name, region);
	});
}

robo.gui.prototype.addRegion = function(name, region){
	this.regions[name] = region;
};

robo.gui.prototype.removeRegion = function(name){
	delete this.regions[name];
};


robo.gui.field = function(){
	this.id = null;
	this.value = null;
};

robo.gui.field.prototype.init = function(id, value, label){
	this.id = id;
	this.value = value;
	this.label = label;
	return this;
}

robo.gui.field.prototype.renderView = function(){
	return "";
};

robo.gui.field.prototype.renderEdit = function(){
	return "";
};

robo.gui.field.prototype.readField = function(html){
	return null;
};

robo.gui.field.prototype.getValue = function(){
	return this.value;
};

robo.gui.fieldTypes = {};
var rgft = robo.gui.fieldTypes;//shorthand

rgft.number =  function(){};

var p = rgft.number.prototype = new robo.gui.field();

p.renderView = function(){
	return $("<p></p>").addClass("field-view-number")
		.append($("<span>").addClass("field-view-number--key").html(this.label+": "))
		.append($("<span>").addClass("field-view-number--value").html(this.getValue()))[0];
};

p.renderEdit = function(){	
	return $("<p></p>").addClass("field-edit-number")
		.append($("<span>").addClass("field-edit-number--key").html(this.label+": "))
		.append($("<input>").addClass("field-edit-number--value").val(this.getValue()))[0];
};

p.readField = function(html){
	return parseFloat($(html).find(".field-edit-number--value").val(), 10);
};



rgft.pattern =  function(){};

var p = rgft.pattern.prototype = new robo.gui.field();

p.getValue = function(){
	return this.value || [];
};

p.renderView = function(){

	var valueHTML = "<ul>"+_.map(this.getValue(), function(item){
		return "<li>"+item+"</li>";
	}).join() + "</ul>";

	return $("<p></p>").addClass("field-view-pattern")
		.append($("<span>").addClass("field-view-pattern--key").html(this.label+": "))
		.append($("<span>").addClass("field-view-pattern--value").html(valueHTML))[0];
};

p.appendPatternEntry = function($container, value){
	$container.append(
		$("<li>").append(
			$("<input>").val(value)
		)
	);
};

p.removePatternEntry = function($container, index){
	index = index || $container.children().length - 1;
	$($container.children()[2]).remove();
};

p.renderEdit = function(){	

	var valueHTML = $("<ul>").addClass("field-edit-pattern--value");
	_.each(this.getValue(), function(item){
		this.appendPatternEntry(valueHTML, item);
	}.bind(this));

	var controlHTML = $("<div>")
		.append($("<button>").addClass('field-edit-pattern--control--add').click(function(){
			this.appendPatternEntry(valueHTML, null);
		}.bind(this)).html('+'))
		.append($("<button>").addClass('field-edit-pattern--control--remove').click(function(){
			this.removePatternEntry(valueHTML, null);
		}.bind(this)).html('-'))



	return $("<p></p>").addClass("field-edit-pattern")
		.append($("<span>").addClass("field-edit-pattern--key").html(this.label+": "))
		.append(valueHTML)
		.append(controlHTML)[0]
};

p.readField = function(html){
	list = $(html).find(".field-edit-pattern--value");
	return _.map(list.children('.li'), function(item){
		// console.log("read item", item);
		return $(item).val();
	});
};








robo.gui.fieldSet = function(){
	this.fields = [];
	this.container = null;
	this.callback = null;
};

robo.gui.fieldSet.prototype.init = function(){
	return this;
}

robo.gui.fieldSet.prototype.buildField = function(setting){
	var type = rgft[setting.type];
	if(type){
		return (new type()).init(setting.id, setting.value, setting.label);
	}
	else{
		return null;
	}

};

robo.gui.fieldSet.prototype.readSettings = function(settings){
	var self = this;
	settings.forEach(function(setting){
		var field = self.buildField(setting);
		if(field){
			self.fields.push(field);
		}
	});
	return this;
};

robo.gui.fieldSet.prototype.setCallback = function(callback){
	this.callback = callback;
	return this;
};

robo.gui.fieldSet.prototype.applySettings = function(){
	var self = this;
	var value = {};
	$.each($(this.container).find(".robogui-field"), function(index, item){
		self.fields[index].value = self.fields[index].readField(item);
		value[self.fields[index].id] = self.fields[index].value;
	});

	if(_.isFunction(this.callback)){
		this.callback(value);
	}
};

robo.gui.fieldSet.prototype.renderInto = function(container, bEdit){
	var self = this,
	$container = $(container);
	this.container = $container;
	$container.html("");

	this.fields.forEach(function(field){
		var $div = $("<div>").addClass("robogui-field");
		var fieldDom = bEdit? field.renderEdit() : field.renderView();
		if(fieldDom){
			$div.append(fieldDom);
		}
		$(container).append($div);

	});
	if(bEdit){
		$container.append($("<button>apply changes</button>").click(self.applySettings.bind(self)));
	}
	else{
		$container.append($("<button>edit</button>").click(self.renderInto.bind(self, container, true)));
	}
};