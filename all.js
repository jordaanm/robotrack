window.robo = window.robo || {};
window.robo.util = window.robo.util || {};

/***************************
		Attempt
***************************/
	robo.attempt = function(level){
		this.level = level;
		this.state = 'idle';
		this.roundCount = 0;
		this.actionQueue = [];
	};

	robo.attempt.prototype.fail = function(){
		if(this.state != "fail"){
			this.state = 'fail';
			console.log("YOU FAIL");
		}
	};

	robo.attempt.prototype.win = function(){
		if(this.state != "win"){
			this.state = 'win';
			console.log("YOU WIN");			
		}
	};

	robo.attempt.prototype.getActions = function(){
		return this.actionQueue || [];
	}

	robo.attempt.prototype.getIncompleteActions = function(){
		var allActions = this.getActions();
		return _.filter(allActions, function(action){
			if(!_.isObject(action)) { return false; }
			return !action.isComplete();
		});
	}

	robo.attempt.prototype.startRound = function(){
		this.actionQueue = [];
		this.roundCount++;
	};

	robo.attempt.prototype.endRound = function(){
	};


window.robo = window.robo || {};
window.robo.util = window.robo.util || {};

/****************************
		ROBOT AVATAR
****************************/

robo.avatar = function(){
	var self = this;
	self.displayObject = null;
	self.coord = {x: 0, y: 0};
	self.state = {};
	self.facing = 0;
	self.setFacing(0);
	self.isDead = false;
	return self;
};

robo.avatar.prototype.setBoard = function(board){
	this.board = board;
};

robo.avatar.prototype.buildDisplayObject = function(state){

	var crono = new createjs.Sprite(robo.util.sprites.crono, "walkRight");
	crono.framerate = 4;
	crono.name = "CronoBot";
	crono.scaleX = crono.scaleY = 0.625;

	var dObj = new createjs.Container();
	dObj.addChild(crono);
	dObj.name = "Avatar";

	return dObj;
};

robo.avatar.prototype.facingMap = {
	0: 'walkRight',
	1: 'walkUp',
	2: 'walkLeft',
	3: 'walkDown',
};

robo.avatar.prototype.facingDisplacement = {
	0: {x: 1, y: 0},
	1: {x: 0, y: -1},
	2: {x: -1, y: 0},
	3: {x: 0, y: 1}
};

robo.avatar.prototype.turnCW = function(turns){
	this.setFacing((this.facing - turns) % 4);
}

robo.avatar.prototype.turnCCW = function(turns){
	this.setFacing((this.facing + turns) % 4);
}

robo.avatar.prototype.getDisplayObject = function(){
	var self = this;
	if(self.displayObject == null){
		self.displayObject = self.buildDisplayObject();
	};
	return (self.displayObject);
};

robo.avatar.prototype.setFacing = function(newFacing){
	//Normalise to 0-4
	while(newFacing < 0){
		newFacing += 4;
	}

	var self = this;
	self.facing = newFacing;
	self.getDisplayObject().children[0].gotoAndPlay(self.facingMap[newFacing]);

};


robo.avatar.prototype.canMoveTo = function(level, x, y){
	// return true;
	var tile = level.board.getTile(x, y);
	return tile && tile.canStepOn();
}

robo.avatar.prototype.tryWalkForward = function(level){
	 var displacement = this.facingDisplacement[this.facing];
	 var x = this.coord.x + displacement.x;
	 var y = this.coord.y + displacement.y;

	if(this.canMoveTo(level, x, y)){
		this.walkForward();
	}
};

robo.avatar.prototype.walkForward = function(){
	var displacement = this.facingDisplacement[this.facing];
	this.coord.x += displacement.x;
	this.coord.y += displacement.y;
};

robo.avatar.prototype.setPositionImmediate = function(x, y){
	var pos = this.board.getTileCenter(x,y);
	var bounds = this.getDisplayObject().getBounds();
	this.getDisplayObject().x = pos.x - bounds.width/2;
	this.getDisplayObject().y = pos.y - bounds.height/2;
	this.coord.x = x; this.coord.y = y;
};

robo.avatar.prototype.startRound = function(args){
	// console.log("ROBO START ROUND", args);
	if(this.isDead){
		args.level.currentAttempt.fail();
	}
	
	this.tryWalkForward(args.level);
	var bounds = this.board.getCoordBounds(this.coord.x, this.coord.y);

	var action = new robo.roundAction.animatePosition(
		this.getDisplayObject(), 
		{x: bounds.left, y: bounds.top}, 
		2.0
	);

	action.setPriority(5);

	args.actions.push(action);
};

robo.avatar.prototype.endRound = function(args){
	this.action = null;
	// console.log("ROBO END ROUND", args);
};

robo.avatar.prototype.update = function(event){

};

robo.avatar.prototype.die = function(){
	this.isDead = true;
};
window.robo = window.robo || {};
window.robo.util = window.robo.util || {};
var p;//prototype shortcut


robo.roundBehaviour = function(){

};

robo.roundBehaviour.prototype.getStateForRound = function(round){
	return this.obj[key];
};

robo.roundBehaviour.prototype.set = function(object, key){
	this.obj = object;
	this.key = key;
	return this;
};

robo.roundBehaviour.prototype.connectSwitchObserver = function(eventer){
};

robo.roundBehaviour.alternate = function(object, key){
	this.set(object, key);
};
p = robo.roundBehaviour.alternate.prototype = Object.create(robo.roundBehaviour.prototype);
p.constructor = robo.roundBehaviour.alternate;

p.getStateForRound = function(round){
	return round%2 ? this.obj[this.key] : !this.obj[this.key];
};

robo.roundBehaviour.pattern = function(object, key){
	this.set(object, key);
};
p = robo.roundBehaviour.pattern.prototype = Object.create(robo.roundBehaviour.prototype);
p.constructor = robo.roundBehaviour.pattern;

p.getStateForRound = function(round){
	var l = this.obj[this.key].length;
	var val = this.obj[this.key][round%l];
	return val;
};


robo.roundBehaviour.switch = function(object, key, channel){
	this.set(object, key);
	this.channel = channel || 0;
};

p = robo.roundBehaviour.switch.prototype = Object.create(robo.roundBehaviour.prototype);
p.constructor = robo.roundBehaviour.switch;

p.getStateForRound = function(round){
	return this.obj[this.key];
};

p.connectSwitchObserver = function(eventer){
	eventer.subscribe('switch', function(data){
		if(data.channel == this.channel){
			this.obj[this.key] = !this.obj[this.key];
		}
	}.bind(this), {}, this);
};
	//Behaviour = switch
	//NOTHING, open/close using connectSwitch


window.robo = window.robo || {};
window.robo.util = window.robo.util || {};

/****************************
		BOARD
****************************/
robo.board = function(width, height){

	this.width = width;
	this.height = height;
	
	this.tiles = [];
	this.defaultTileName = "vacant";

	this.effects = [];

	this.avatars = [];

	this.displayObject = null;
	this.tilesize = robo.settings.tilesize;
	this.tileGap = robo.settings.tileGap;
	
	this.eventer = new robo.util.observer();

	this.init(width, height);

	return this;
};

robo.board.prototype.getSettings = function(){
	return [
		{type: 'number', value: this.getDisplayObject().scaleX, id: 'scale', label: 'scale'},
		{type: 'number', value: this.width, id: 'width', label: 'width'},
		{type: 'number', value: this.height, id: 'height', label: 'height'}
	];
};

robo.board.prototype.applySettings = function(settings){
	var newWidth = settings.width;
	var newHeight = settings.height;
	if(newWidth != this.width || newHeight != this.height){
		this.setDimensions(newWidth, newHeight);
	}
	if(settings.scale){
		this.getDisplayObject().scaleX = this.getDisplayObject().scaleY = settings.scale;	
	}
};

robo.board.prototype.edit = function(){
	var dialog = new robo.dialogs.editBoardDialog();
	dialog.open(this, this.applySettings.bind(this));
};

robo.board.prototype.exportMap = function(){
	return {
		width: this.width,
		height: this.height,
		default: this.defaultTileName,
		tiles: _.map(this.tiles, function(tile){
		return tile.export();
		})
	};
};

robo.board.prototype.init = function(width, height){
	this.avatarContainer = new createjs.Container();
	this.tileContainer = new createjs.Container();
	this.effectContainer = new createjs.Container();

	this.avatarContainer.name = "Avatar Container";
	this.setDimensions(width, height);
	return this;
};

robo.board.prototype.update = function(deltaTime, timer){
	var self = this;
	//Update Tiles
	this.tiles.forEach(function(tile){
		tile.update(deltaTime, timer);
	});

	//Update Avatars
	this.avatars.forEach(function(avatar){
		avatar.update(deltaTime, timer);
	});
};

robo.board.prototype.startRound = function(args){

	// args = $.extend(true, { board: this}, args);
	args.board = this;

	this.tiles.forEach(function(tile){
		tile.startRound(args);
	});

	// this.effects.forEach(function(effect){
	// 	effect.startRound(args);
	// });

	this.avatars.forEach(function(avatar){
		avatar.startRound(args);
	});
};

robo.board.prototype.endRound = function(args){

	this.avatars.forEach(function(avatar){
		avatar.endRound(args);
	});

	// console.log("EEE", this.effects);
	// this.effects.forEach(function(effect){
	// 	effect.endRound(args);
	// });
	
	this.tiles.forEach(function(tile){
		tile.endRound(args);
	});
};

robo.board.prototype.dropCardAtLocal = function(card, local){
	var self = this;
	var retTile = null;
	self.tiles.forEach(function(tile){
		var pt = tile.getDisplayObject().localToLocal(25, 25, card.getDisplayObject());
		if(pt.y < 50 && pt.y > 0 && pt.x < 50 && pt.x > 0){
			retTile = tile;
		}
	});
	return retTile;
};

robo.board.prototype.dropTileAtLocal = function(tileDO, local){
	var self = this;
	var retTile = null;
	self.tiles.forEach(function(tile){
		var pt = tile.getDisplayObject().localToLocal(25, 25, tileDO);
		if(pt.y < 50 && pt.y > 0 && pt.x < 50 && pt.x > 0){
			retTile = tile;
		}
	});
	return retTile;
};

robo.board.prototype.getTile = function(x, y){
	if(x >= this.width || x < 0 || y >= this.height || y < 0){
		return null;
	}
	else{
		return this.tiles[this.width*y+x];
	}
};

robo.board.prototype.removeTile = function(x,y){
	var tile = this.tiles[this.getIndexFromCoord(x,y)];
	if(tile){
		this.getDisplayObject().removeChild(tile.getDisplayObject());
		this.tiles[this.getIndexFromCoord(x,y)] = null;
	}
};

robo.board.prototype.setTile = function(x,y,defn, settings){

	//Remove existing tile
	this.removeTile(x,y);
	var tile = new robo.tile(defn,x,y);
	this.tiles[this.getIndexFromCoord(x,y)] = tile;
	var dObj = tile.getDisplayObject();

	dObj.x = (this.tilesize+this.tileGap)*x;
	dObj.y = (this.tilesize+this.tileGap)*y;
	this.tileContainer.addChild(dObj);

	tile.loadSettings(settings);	

	this.getDisplayObject().setChildIndex(this.avatarContainer, this.getDisplayObject().children.length-2);
};

robo.board.prototype.setDimensions = function(width, height){
	this.emptyBoard();
	this.width = width;
	this.height = height;
	this.tiles = new Array(width * height);
	_.each(this.tiles, function(tile, index){
		var coord = this.getCoordFromIndex(index);
		this.setTile(coord.x, coord.y, robo.util.tileDfns[this.defaultTileName]); 
	}.bind(this));
};

robo.board.prototype.emptyBoard = function(){
	var self = this;
	this.tiles.forEach(function(tile, index){
		self.removeTile(self.getCoordFromIndex(index));
	});
};

robo.board.prototype.fillBoard = function(arrDefn){
	var self = this;
	arrDefn.forEach(function(data, index){
		var coord = self.getCoordFromIndex(index);
		self.setTile(coord.x, coord.y, data.definition, data.settings);
	});
};

robo.board.prototype.getIndexFromCoord = function(x, y){
	if(typeof x == "object"){
		y = x.y;
		x = x.x;
	}
	return this.width*y+x;
};

robo.board.prototype.getCoordFromIndex = function(i){
	y = ~~(i/this.width);
	x = i % this.width;
	return {x: x, y: y};
};

robo.board.prototype.buildDisplayObject = function(){
	displayObject = new createjs.Container();
	displayObject.name = "Board";
	displayObject.addChild(this.tileContainer);
	displayObject.addChild(this.avatarContainer);
	displayObject.addChild(this.effectContainer);
	return displayObject;
};

robo.board.prototype.getDisplayObject = function(){
	return (this.displayObject = this.displayObject || this.buildDisplayObject());
};

robo.board.prototype.getCoordBounds = function(x, y){
	return {
		top: y*(this.tilesize + this.tileGap),
		left: x*(this.tilesize + this.tileGap),
		width: this.tilesize,
		height: this.tilesize
	};
};

robo.board.prototype.getTileCenter = function(x, y){
	var bounds = this.getCoordBounds(x, y);
	return {
		x: bounds.left + bounds.width/2,
		y: bounds.top + bounds.height/2
	};
};

robo.board.prototype.addAvatar = function(avatar){
	this.avatars.push(avatar);
	avatar.setBoard(this);
	var dObj = avatar.getDisplayObject();
	this.resizeToFitTile(dObj);
	this.avatarContainer.addChild(dObj);
};

robo.board.prototype.resizeToFitTile = function(displayObject){
	var bounds = displayObject.getBounds();
	displayObject.scaleX = this.tilesize / bounds.width;
	displayObject.scaleY = this.tilesize / bounds.height;
};

robo.board.prototype.connectSwitchObservers = function(){
	_.each(this.tiles, function(tile){
		tile.connectSwitchObservers(this);
	}.bind(this));
};

robo.board.prototype.addEffect = function(effect){
	this.effects.push(effect);
	this.effectContainer.addChild(effect.getDisplayObject());

	return effect;
};

robo.board.prototype.removeEffect = function(effect){
	var i = this.effects.indexOf(effect);
	if(i > -1){
		this.effectContainer.removeChild(effect.getDisplayObject());
		this.effects.splice(i,1);
	}
}

robo.board.prototype.scanTilesForField = function(x, y, direction, property){
	var cardinal = {
		x: Math.cos(direction*Math.PI/2),
		y: -1*Math.sin(direction*Math.PI/2)
	};


	var scanX = x, scanY = y;

	while(scanX >= 0 && scanX <= this.width 
		&& scanY >= 0 && scanY <= this.height){
		var tile = this.getTile(scanX, scanY);
		if(tile && tile.getFieldEffects()[property]){
			return {
				x: scanX,
				y: scanY,
				found: true,
				tile: tile
			};
		}
		scanX += cardinal.x; scanY += cardinal.y;
	}

	return {
		x: scanX, 
		y: scanY, 
		tile: null,
		found: false
	};
};
window.robo = window.robo || {};
window.robo.util = window.robo.util || {};

/****************************
		Card
****************************/
robo.cardDfn = function(id, action, icon, color){
	this.icon = icon;
	this.action = action;
	this.id = id;
	this.color = color;

	return this;
}


robo.card = function(cardDfn, hand){
	var self = this;
	this.hand = hand;
	this.tapped = false;
	this.definition = cardDfn;
}

robo.card.prototype.getHand = function(){
	return this.hand;
}

robo.card.prototype.attachTo = function(tile){
	if(!this.tapped){
		this.tapped = true;
		tile.attachCard(this);
	}
}

robo.card.prototype.getDisplayObject = function(){
	var self = this;
	self.state = self.state || {};
	if(!self.displayObject){
		self.displayObject = new createjs.Container();
		//Create card backing
		var graphics = new createjs.Graphics();
		var backing = new createjs.Shape(graphics);
		backing.graphics
			.beginStroke("#000")
			.beginFill("#fff")
			.drawRoundRect(0,0,50,50,5);
		self.displayObject.addChild(backing);
		 //Create icon
		var icon = self.definition.icon.clone(), 
		iconBounds = icon.getBounds();
		
		icon.x = 25 - iconBounds.width/2;
		icon.y = 25 - iconBounds.height/2;
		self.displayObject.addChild(icon);
		
		 // Create count
		 // self.displayObject
		
		 //Add touch events
		 self.displayObject.addEventListener('rollover', function(event){
		 	//grow slightly
		 	self.displayObject.scaleX = self.displayObject.scaleY =1.1;
		 });
		 self.displayObject.addEventListener('rollout', function(event){
		 	// return to normal size
		 	self.displayObject.scaleX = self.displayObject.scaleY = 1.0
		 });
		 self.displayObject.addEventListener('mousedown', function(event){
		 	self.drag(event);
		 });
		 self.displayObject.addEventListener('pressup', function(event){
		 	self.drop(event);
		 });
		 self.displayObject.addEventListener('pressmove', function(event){
		 	self.dragMove(event);
		 });
	}

	return this.displayObject;
}

robo.card.prototype.getFieldEffects = function(tile){
	return {
		blockLaser: false
	};
};

robo.card.prototype.update = function(){
	this.getDisplayObject().alpha = this.tapped ? 0 : 100;
};

robo.card.prototype.drag = function(event){
	var self = this;
	self.state = self.state||{};
	self.state.drag = true;
	self.state.dragOffset = {
		x: self.displayObject.x - event.stageX,
		y: self.displayObject.y - event.stageY
	};
}

robo.card.prototype.drop = function(event){
	var self = this;
	self.state = self.state||{};
	if(self.state.drag){
		self.state.drag = false;
		var b = self.displayObject.getBounds();

		var tile = self.getHand().getLevel().dropCardAtLocal(self, {x: b.width/2, y:b.height/2});
		if(tile){
			self.getHand().getLevel().playCard(self, tile);
		}
	}
}
robo.card.prototype.dragMove = function(event){
	var self = this;
	self.state = self.state||{};
	if(self.state.drag){
		self.displayObject.x = event.stageX + self.state.dragOffset.x;
		self.displayObject.y = event.stageY+ self.state.dragOffset.y;
	}
}



/****************************
		Card Definitions
****************************/

	robo.util.cardDfns = {};
window.robo = window.robo || {};
window.robo.util = window.robo.util || {};

var p; // prototype shortcut
/****************************
		ASSET COLLECTIONS
****************************/

robo.collection = function(){
	this.items = {};
	this.name = "";
	this.library = null;
	this.resources = [];
};

robo.collection.prototype.init = function(library){
	this.library = library;
	return this;
};

robo.collection.prototype.add  =function(name, value){
	this.items[name] = value;
};

robo.collection.prototype.remove = function(name){
	delete this.items[name];
};

robo.collection.prototype.get = function(name){
	return this.items[name];
}

robo.collection.prototype.getResources = function(){

};

robo.collection.prototype.parse = function(data){

};

robo.collection.prototype.loadResource = function(id, data, settings){
	var converted = this.parse(data, settings);
	this.items[id] = converted;
}

robo.collection.spriteCollection = function(){
	this.name = "sprite";
};

robo.collection.spriteCollection.prototype = new robo.collection();
robo.collection.spriteCollection.prototype.constructor = robo.collection.spriteCollection;
p = robo.collection.spriteCollection.prototype;

p.parse = function(data, settings){
	var obj = $.extend(true, 
		{
			images: [data]
		}, 
		{
			"frames": {
				regX: 0,
				regY: 0,
				height: 50,
				width: 50,
				count: 1
			},
			"animations": {"idle": [0,0,"idle", 1.0]}
		},
		settings
	);

	return new createjs.SpriteSheet(obj);
};

robo.collection.iconCollection = function(){
	this.name = "icon";
};
robo.collection.iconCollection.prototype = new robo.collection();
robo.collection.iconCollection.prototype.constructor = robo.collection.iconCollection;
p = robo.collection.iconCollection.prototype;

p.parse = function(data, settings){
	return new createjs.Bitmap(data)
};
window.robo = window.robo || {};
window.robo.util = window.robo.util || {};
var p;//prototype shortcut


robo.dialog = function(){
	this.init();
};

robo.dialog.prototype.init = function(){
	this.headerTitle = ko.observable("Dialog Title");
	this.dialogWidth = ko.observable("500px");
}

robo.dialog.prototype.getContainer = function(){
	return robo.dialog.$container = robo.dialog.$container || $("<div>").addClass("dialog-container").appendTo($(".main"));
}

robo.dialog.prototype.getTemplate = function(){
	if(!this.template){ return null;}
	return $("#"+this.template).html();
}

robo.dialog.prototype.open = function(){

	var $container = this.getContainer();
	this.modalOverlay = $("<div>").addClass("modal-overlay")
	var instanceBody = $("<div>")
		.addClass('dialog-instance-drag')
		.attr("data-bind", "template:{name:'dialog-instance', data: $data}")
		.appendTo(this.modalOverlay);
		// .drags({ handle: '.dialog-header'});

	this.onOpen.apply(this,arguments);

	this.modalOverlay.appendTo($container).css("top", "0")
	ko.applyBindings(this, instanceBody[0]);

	_.delay(function(){
		instanceBody.drags({handle: '.dialog-header'});
		instanceBody.css({
			top: "100px",
			left: "100px"
		});
	}, 0);
};

robo.dialog.prototype.onOpen = function(){};
robo.dialog.prototype.onClose = function(){};

robo.dialog.prototype.close = function(){
	this.onClose.apply(this, arguments);

	this.modalOverlay.remove();
};

robo.dialogs = {};

robo.dialogs.editBoardDialog = function(){
	this.init();
	this.template = 'dialog-board-edit';
};

p = robo.dialogs.editBoardDialog.prototype = Object.create(robo.dialog.prototype);
p.constructor = robo.dialogs.editBoardDialog;

p.onOpen = function(board, callback){
	console.log('hi', board);
	this.callback = callback;
	this.height = ko.observable(board.height);
	this.width = ko.observable(board.width);
	this.scale = ko.observable(board.getDisplayObject().scaleX);
};

p.save = function(){
	console.log("THIS", this);
	var data = {
		scale: this.scale(),
		width: this.width(),
		height: this.height()
	};

	if(_.isFunction(this.callback)){
		this.callback(data);
	}

	this.close();
};
(function($) {
    $.fn.drags = function(opt) {

        opt = $.extend({handle:"",cursor:"move"}, opt);

        if(opt.handle === "") {
            var $el = this;
        } else {
            var $el = this.find(opt.handle);
        }

        return $el.css('cursor', opt.cursor).on("mousedown", function(e) {
            if(opt.handle === "") {
                var $drag = $(this).addClass('draggable');
            } else {
                var $drag = $(this).addClass('active-handle').parent().addClass('draggable');
            }
            var z_idx = $drag.css('z-index'),
                drg_h = $drag.outerHeight(),
                drg_w = $drag.outerWidth(),
                pos_y = $drag.offset().top + drg_h - e.pageY,
                pos_x = $drag.offset().left + drg_w - e.pageX;
            $drag.css('z-index', 1000).parents().on("mousemove", function(e) {
                $('.draggable').offset({
                    top:e.pageY + pos_y - drg_h,
                    left:e.pageX + pos_x - drg_w
                }).on("mouseup", function() {
                    $(this).removeClass('draggable').css('z-index', z_idx);
                });
            });
            e.preventDefault(); // disable selection
        }).on("mouseup", function() {
            if(opt.handle === "") {
                $(this).removeClass('draggable');
            } else {
                $(this).removeClass('active-handle').parent().removeClass('draggable');
            }
        });

    }
})(jQuery);
window.robo = window.robo || {};
window.robo.util = window.robo.util || {};

robo.fieldEffect = function(){

};

robo.fieldEffect.prototype.startRound = function(){};
robo.fieldEffect.prototype.endRound = function(){};

robo.fieldEffect.prototype.tryAffect = function(avatar){
	if(this.inAffectedArea(avatar.coord.x, avatar.coord.y)){
		this.applyEffect(avatar);
	}
};

robo.fieldEffect.prototype.applyEffect = function(avatar){
	avatar.die();
};

robo.fieldEffect.prototype.inAffectedArea = function(x, y){
	return false;
};

robo.fieldEffect.beam = function(board, tile){
	this.board = board;
	this.tile = tile;
	this.active = false;
	this.direction = 0;
};

p = robo.fieldEffect.beam.prototype = Object.create(robo.fieldEffect.prototype);
p.constructor = robo.fieldEffect.beam;

p.setAttr = function(attr){
	this.prevDirection = this.direction;
	this.prevActive = this.active;

	this.direction = attr.direction;
	this.active = attr.active;
}

p.animateBeam = function(callback){
	var shape = this.getDisplayObject(true);
	if(_.isFunction(callback)){
		callback();
	}
};

p.getEndCoords = function(){
		var direction = {
			x: Math.sin(this.direction*Math.PI/2),
			y: Math.cos(this.direction*Math.PI/2)
		};

		var scanResults = this.board.scanTilesForField(this.tile.coord.x, this.tile.coord.y, this.direction, 'blockLaser');

		return {
			x: scanResults.x,
			y: scanResults.y
		};
	};

p.getDisplayObject = function(forceUpdate){
	if(!this.shape || forceUpdate){
		this.shape = this.shape || new createjs.Shape();

		if(forceUpdate){ this.shape.graphics.clear(); }

		var pos = this.board.getTileCenter(this.tile.coord.x, this.tile.coord.y),
		endCoords = this.getEndCoords(),
		endPos = this.board.getTileCenter(endCoords.x, endCoords.y);

		this.shape.graphics.beginStroke("#ff0000").moveTo(pos.x,pos.y).lineTo(endPos.x,endPos.y);
	}
	this.shape.visible = !!this.active;

	return this.shape;
};

p.startRound = function(args){
	console.log("EFFECT START", args);
};

p.endRound = function(args){
	//If robot in our field,  kill it
	console.log("EFFECT END", args);
	// if()
};

p.inAffectedArea = function(x, y){
	if(!this.active) { return false; }
	var endCoords = this.getEndCoords(),
	startCoords = this.tile.coord,
	minX = Math.min(startCoords.x, endCoords.x),
	maxX = Math.max(startCoords.x, endCoords.x),
	minY = Math.min(startCoords.y, endCoords.y),
	maxY = Math.max(startCoords.y, endCoords.y);


	switch(this.direction % 4){
		case 0:
		case 2:
			return y == startCoords.y && y >= minY && y <= maxY;
		case 1:
		case 3:
			return x == startCoords.x && y >= minY && y <= maxY;
		default:
			return false;
	}
};
window.robo = window.robo || {};
window.robo.util = window.robo.util || {};

/*****************************
		ENGINE
*****************************/

robo.engine = function(){
	this.level = null;
	this.gui = null;
	this.library = null;
	this.stage = null;
};

robo.engine.prototype.init = function(stage, regions, assets){
	this.stage = stage;
	this.gui = (new robo.gui()).init(regions);
	this.library = (new robo.library(assets)).init();

	return this;
};

robo.engine.prototype.loadLevel = function(levelJson){
	if(_.isString(levelJson)) { levelJson = JSON.parse(levelJson); }

	if(this.level){
		this.stage.removeChild(this.level.getDisplayObject());
		this.level = null;
	}

	this.level = (new robo.level()).init();
	var a = this.level.loadBoard(levelJson.map)
	var b = this.level.loadCards(levelJson.cards);
	
	this.stage.addChild(this.level.getDisplayObject());
	
	return this;
};

robo.engine.prototype.exportLevel = function(){
	if(this.level){
		return {
			map: this.level.exportBoard(),
			cards: this.level.exportCards()
		};
	}
};

robo.engine.prototype.exportLevelToString = function(){
	return JSON.stringify(this.exportLevel());
}

robo.engine.prototype.update = function(event){
	this.stage.update(event);
};

window.robo = window.robo || {};
window.robo.util = window.robo.util || {};

robo.hand = function(level){
	this.cards = [];
	this.level = level;
}


robo.hand.prototype.getLevel = function(level){
	return this.level;
};

robo.hand.prototype.fillHand = function(cardDfns){
	var self = this;
	cardDfns.forEach(function(cardDfn){
		self.addCard(new robo.card(cardDfn, self));
	});
};

robo.hand.prototype.emptyHand = function(){
	var self = this;
	this.cards.forEach(function(card){
		self.removeCard(card);
	})
}


robo.hand.prototype.addCard = function(card){
	this.cards.push(card);

	var cardDO = card.getDisplayObject();
	this.getDisplayObject().addChild(cardDO);
	var index = this.cards.length - 1;
	cardDO.x = 60*(index % 4);
	cardDO.y = 60* (Math.floor(index/4));
};

robo.hand.prototype.removeCard = function(card){
	var index = this.cards.indexOf(card);
	if(index > -1){
		var cardDO = card.getDisplayObject();
		this.getDisplayObject().removeChild(cardDO);
		this.cards.splice(index, 1);
	}
}

robo.hand.prototype.getDisplayObject = function(){
	var self = this;
	if(!this.displayObject){
		this.displayObject = new createjs.Container();
	}

	return this.displayObject;
};

robo.hand.prototype.update = function(){
	// this.getDisplayObject().update();
	this.cards.forEach(function(card){
		card.update();
	});
}
window.robo = window.robo || {};
window.robo.util = window.robo.util || {};

/****************************
		Level
****************************/

	robo.level = function(board, cardDfns){
		// Setup board
		this.board = null;
		this.round = 0;

		//Setup hand
		this.hand = new robo.hand(this);
		this.cardDfns = null;

		this.editorHand = new robo.tileHand(this);

		this.simStarted = false;

		this.phases = [
			robo.util.phases.loadLevel,
			robo.util.phases.playChips,
			robo.util.phases.runSimulation
		];


		this.currentPhase = 0;

		this.currentAttempt = null;
		this.previousAttempts = [];

		// this.init();
	};

	robo.level.prototype.init = function(){
		this.currentAttempt = new robo.attempt(this);
		this.phases[this.currentPhase].onEvent("startPhase", {level: this});
		return this;
	};

	robo.level.prototype.loadCards = function(cardDfnsJson){
		this.cardDfns = _.map(cardDfnsJson, function(name){
			return robo.util.cardDfns[name];
		});
		return this;
	};

	
	robo.level.prototype.loadBoard = function(boardJson){
		this.board = robo.util.loadMapJson(boardJson);
		return this;
	};

	robo.level.prototype.exportBoard = function(){
		return this.board.exportMap();
	};

	robo.level.prototype.exportCards = function(){
		return _.map(this.cardDfns, function(dfn){
			return _.findKey(robo.util.cardDfns, dfn);
		});
	};

	robo.level.prototype.startAttempt = function(){
		if(this.currentAttempt){
			this.previousAttempts.push(currentAttempt);
		}
		this.currentAttempt = new robo.attempt(this);
	};

	robo.level.prototype.dropCardAtLocal = function(card, local){
		return this.board.dropCardAtLocal(card, local);
	};

	robo.level.prototype.dropTileAtLocal = function(tileDO, local){
		return this.board.dropTileAtLocal(tileDO, local);
	};

	robo.level.prototype.playCard = function(card, tile){
		return this.phases[this.currentPhase].onEvent('playCard', {
			level: this,
			card: card,
			tile: tile
		});
	};

	robo.level.prototype.placeTile = function(tileDfn, tile){
		return this.phases[this.currentPhase].onEvent('placeTile', {
			level: this,
			tileDfn: tileDfn,
			tile: tile
		});
	};

	robo.level.prototype.selectTile = function(tileDfn, tile){
		return this.phases[this.currentPhase].onEvent('selectTile', {
			level: this,
			tileDfn: tileDfn,
			tile: tile
		});
	};

	robo.level.prototype.nextPhase = function(){
		if(this.phases.length > (this.currentPhase +1)){

			this.phases[this.currentPhase].onEvent("endPhase", {level: this});
			this.currentPhase++;
			this.phases[this.currentPhase].onEvent("startPhase", {level:this});
		}
	}

	robo.level.prototype.getDisplayObject = function(){
		var self = this;
		if(!this.displayObject){
			this.displayObject = new createjs.Container();
			var b = this.board.getDisplayObject();
			var h = this.hand.getDisplayObject();
			var e = this.editorHand.getDisplayObject();
			h.x = 550;
			h.y = 250;
			e.x = 550;
			e.y = 250;
			this.displayObject.addChild(b);
			this.displayObject.addChild(h);
			this.displayObject.addChild(e);

			this.displayObject.addEventListener("tick", function(event){
				self.update.call(self, event.params[0]);
			} );

		}

		return this.displayObject;
	}

	robo.level.prototype.showEditorCards = function(){
		var dfns = [];
		for(name in robo.util.tileDfns){
			dfns.push(robo.util.tileDfns[name]);
		}
		this.editorHand.fillHand(dfns);
	};

	robo.level.prototype.hideEditorCards = function(){
		this.editorHand.emptyHand();
	};

	robo.level.prototype.update = function(timer){
		var self = this;
		window.timer = timer;
		this.phases[this.currentPhase].update(self, timer);
		
		self.board.update(timer);
		self.hand.update(timer);
	}

	robo.level.prototype.getIncompleteActions = function(){
		if(this.currentAttempt){
			return this.currentAttempt.getIncompleteActions();
		}
		else{
			return [];
		}
	};

	robo.level.prototype.startRound = function(){
		this.currentAttempt.startRound();

		this.board.startRound({
			level: this,
			round: this.currentAttempt.roundCount,
			actions: this.currentAttempt.getActions()
		});
	};	

	robo.level.prototype.endRound = function(){
		this.currentAttempt.endRound();
		this.board.endRound({
			round: this.currentAttempt.roundCount,
			actions: this.currentAttempt.getActions()
		});
	};


/*****************************
		RESOURCE LIBRARY
*****************************/

robo.library = function(){
	this.collections = {};
	this.resources = [];
};

robo.library.prototype.init = function(){
	this.collections.sprite = (new robo.collection.spriteCollection()).init(this);
	this.collections.icon = (new robo.collection.iconCollection()).init(this);
	this.loader = new createjs.LoadQueue(false);
	return this;
};


robo.library.prototype.getLoader = function(){ return this.loader; };

robo.library.prototype.addResource = function(data){
	this.resources.push(data);
};

robo.library.prototype.addResources = function(arrData){
	_.each(arrData, this.addResource.bind(this));
};

robo.library.prototype.getManifest = function(){
	return _.map(this.resources, function(item){
		return {
			src: item.src,
			id: item.id
		};
	})
};

robo.library.prototype.load = function(){
	var manifest = this.getManifest();
	this.loader.loadManifest(manifest);
};	

robo.library.prototype.parseAssets = function(loader){
	var keys = _.keys(loader._loadedResults);
	var self = this;
	_.each(keys, function(key){
		var parts = key.split(".");
		if(parts.length > 1){
			var section = parts[0],
			id = parts[1];

			var options = (_.findWhere(self.resources, {id: key}) || {}).options || {};

			if(self.collections[section]){
				self.collections[section].loadResource(id, loader.getResult(key), options);
			}
		}
	})
};

robo.library.prototype.get = function(section, id){
	if(this.collections[section]){
		return this.collections[section].get(id);
	}else{
		return null;
	}
};

window.robo = window.robo || {};
window.robo.util = window.robo.util || {};
var p;

robo.util.observer = function(){
	this.events = {};
};

p = robo.util.observer.prototype;

p.subscribe = function(eventName, cb, data, context){
	this.getEvent(eventName).push({callback:cb, data: data, context: context});
	return this;
};

p.unsubscribe = function(eventName, callback){
	var index = _.chain(this.getEvent)
	.pluck('callback')
	.indexOf(callback)
	.value();
	this.getEvent(eventName).splice(index, 1);

	return this;
};

p.trigger = function(eventName, eventData){
	_.each(this.getEvent(eventName), function(sub, index){
		var finalData = $.extend(true, {data: sub.data}, eventData);
		sub.callback.call(sub.context, finalData);
	});
	return this;
};

p.getEvent = function(eventName){
	this.events[eventName] = this.events[eventName] || [];
	return this.events[eventName];
};
window.robo = window.robo || {};
window.robo.util = window.robo.util || {};
var p;//prototype shortcut

/***************************
   PHASES
**************************/

// Phases
robo.phase = function(){
	this.id = null;
	this.name = null;
	this.eventHandler = null;
	this.updateFn = null;
	this.activeTool = null;
};

p = robo.phase.prototype;

p.getAvailableTools = function(){ return []; };

p.initToolbar = function(){
	var bar = $('.phase-buttons').empty();

	this.tools = this.getAvailableTools();
	_.each(this.tools, function(tool){
		bar.append($("<button>")
			.addClass("btn-phase-tool")
			.html(tool.name)
			.click(function(){
				this.setTool(tool);
			}.bind(this))
		);
	}.bind(this));

};

p.setTool = function(newTool){

	if(this.activeTool){
		this.activeTool.deactivate();
	}

	this.activeTool = newTool;
	this.activeTool.activate();
};

p.onEvent = function(eventType, eventDetails){
	if(this.eventHandler){
		return this.eventHandler(eventType, eventDetails);
	}
	else{
		return false;
	}
};

p.update = function(level, timer){};

p.handleActions = function(level, timer){
	if(level.getIncompleteActions().length == 0){
		level.endRound();
		level.startRound();
	}

	var actions = level.getIncompleteActions();
	var lowestPriority = _.min(actions, 'priority').priority;

	_.each(actions, function(action){
		if(action.priority == lowestPriority){
			action.update(timer.delta/1000);
		}
	});

	// if(level.getIncompleteActions().length == 0){
	// 	level.endRound();
	// }		
};

p.connectSwitchObservers = function(level){
	level.board.connectSwitchObservers();
};

robo.phase.loadLevel = function(){
	this.id = "load";
	this.name = "Load Level";
};

p = robo.phase.loadLevel.prototype = Object.create(robo.phase.prototype);

p.eventHandler = function(eventType, eventDetails){
	if(eventType == 'startPhase'){
		//Show 1 of each tile type
		this.initToolbar();
		eventDetails.level.showEditorCards();
	}
	if(eventType == 'playCard'){

	}
	if(eventType == 'selectTile'){
		if(this.activeTool){
			this.activeTool.select('tile', eventDetails.tile, eventDetails);
		}
	}
	if(eventType == 'placeTile'){
		var x = eventDetails.tile.coord.x,
		y = eventDetails.tile.coord.y;
		eventDetails.level.board.setTile(x,y, eventDetails.tileDfn);
	}
	if(eventType == 'endPhase'){
		//Remove tile types
		eventDetails.level.hideEditorCards();
	}
};

p.getAvailableTools = function(){
	return [
		new robo.tools.inspect()
	];
};


robo.phase.playChips = function(){
	this.id = "chip";
	this.name = "Place Chips";
};

p = robo.phase.playChips.prototype = Object.create(robo.phase.prototype);

p.eventHandler = function(eventType, eventDetails){
	if(eventType == "startPhase"){
		eventDetails.level.hand.fillHand(eventDetails.level.cardDfns);
	}
	else if(eventType == "endPhase"){
		eventDetails.level.hand.cards.forEach(function(card){
			card.tapped = true;
		});				
	}
	else if(eventType == "playCard"){
		eventDetails.card.attachTo(eventDetails.tile);
	}
};

p.update = function(level, timer){
	this.handleActions(level, timer);
};

robo.phase.runSimulation = function(){
	this.id = "run";
	this.name = "Run Simulation";
};

p = robo.phase.runSimulation.prototype = Object.create(robo.phase.prototype);

p.eventHandler = function(eventType, eventDetails){
	if(eventType == "startPhase"){			
		eventDetails.level.simStarted = true;
		eventDetails.level.currentRound = eventDetails.level.round = 0;
		// eventDetails.level.startAttempt();
		this.initialTime = createjs.Ticker.getTime();

		eventDetails.level.board.tiles.forEach(function(tile){
			if(tile.tileDfn === robo.util.tileDfns.avatarStartN){
				var av = new robo.avatar();
				eventDetails.level.board.addAvatar(av);
				av.setPositionImmediate(tile.coord.x, tile.coord.y);
			}
		});

		this.connectSwitchObservers(eventDetails.level);
	}
};

p.update = function(level, timer){
	this.handleActions(level, timer);
};


/***************************
   PHASES
**************************/

	robo.util.phases = {};
	robo.util.phases.loadLevel = new robo.phase.loadLevel();
	robo.util.phases.playChips = new robo.phase.playChips();
	robo.util.phases.runSimulation = new robo.phase.runSimulation();
window.robo = window.robo || {};
window.robo.util = window.robo.util || {};
var p;//prototype shortcut

robo.roundAction = function(){
	this.bComplete = false;
	this.queue = null;
	this.priority = 5;
};

p = robo.roundAction.prototype;

p.complete = function(){
	this.bComplete = true;
};

p.update = function(){};

p.isComplete = function(){
	return !!this.bComplete;
};

p.setPriority = function(newPriority){
	this.priority = newPriority;
};

robo.roundAction.animatePosition = function(displayObject, targetPos, baseDuration){
	this.animProgress = 0;
	this.baseDuration = baseDuration;
	this.displayObject = displayObject;
	this.targetPos = targetPos;
	this.origPos = {x: this.displayObject.x, y: this.displayObject.y};
};

p = robo.roundAction.animatePosition.prototype = Object.create(robo.roundAction.prototype);

p.update = function(deltaTime){

	this.animProgress += deltaTime;//todo: get playback rate, multiply;
	
	var progress = this.animProgress/this.baseDuration;
	if(progress < 1.0){
		this.displayObject.x = (this.origPos.x + ((this.targetPos.x - this.origPos.x) * progress));
		this.displayObject.y = (this.origPos.y + ((this.targetPos.y - this.origPos.y) * progress));
	}
	else{
		this.complete();
	}
};

robo.roundAction.animateSprite = function(sprite, animation, framerateOverride){
	this.sprite = sprite;
	sprite.gotoAndPlay(animation);

	if(framerateOverride){
		this.originalFramerate = sprite.framerate;
		sprite.framerate = framerateOverride;
	}

	this.sprite.on("animationend", this.complete.bind(this));
};

p = robo.roundAction.animateSprite.prototype =  Object.create(robo.roundAction.prototype);
p.constructor = robo.roundAction.animateSprite;

p.complete = function(){
	this.bComplete = true;
	if(this.sprite){
		if(this.originalFramerate){
			this.sprite.framerate = this.originalFramerate;
		}
		this.sprite.off("animationend", this.complete);
		this.sprite = null;	
	}
};

robo.roundAction.beam = function(effect){
	this.effect = effect;
	this.priority = 2;
};


p = robo.roundAction.beam.prototype =  Object.create(robo.roundAction.prototype);
p.constructor = robo.roundAction.beam;

p.update = function(deltaTime){
	this.effect.animateBeam(this.complete.bind(this));
}
$(function(){
	$(".slide-menu .corner-icon").click(function(){
		$(".slide-menu").toggleClass('active');
	})
});
window.robo = window.robo || {};
window.robo.util = window.robo.util || {};
var p;//prototype shortcut
/****************************
		TILE DEFINITION
****************************/
robo.tileDfn = function(){
	this.spriteId = null;
	this.iconId = null;
	this.defaultAnimation = void 0;
	this.name = "BLANK DFN";
	return this;
};

p = robo.tileDfn.prototype;
p.buildResources = function(library){
	this.icon = library.get("icon", this.iconId);
	this.sprite = new createjs.Sprite(library.get("sprite", this.spriteId), this.defaultAnimation);
};

p.editTemplate = "edit-tile-value-template";

p.render = function(tile){
	var container = tile.getDisplayObject();
	tile.sprite = this.sprite.clone();
	container.addChild(tile.sprite);
};

p.getEffect = function(tile){ return tile.effect || false; }

p.startRound = function(args, tile){};
p.endRound = function(args, tile){};

p.update = function () {};
p.canStepOn = function() { return true; };
p.onTouch = function(avatar, tile, board){};

p.getEditSettings = function(){ return []; };
p.loadSettings = function(tile, settings){};
p.exportSettings = function(tile){return {}};

p.connectSwitchObserver = function(tile, board){};

p.getFieldEffects = function(){
	return {};
};

p.getDefaultSettings = function(){
	return {};
};
//Definitions

/*********************
        VACANT
**********************/

robo.tileDfn.vacant = function(){
	this.iconId = "tile-icon";
	this.spriteId = "tile";
	this.defaultAnimation = "idle";
	this.name = "Vacant";
};

p = robo.tileDfn.vacant.prototype = Object.create(robo.tileDfn.prototype);
p.constructor = robo.tileDfn.vacant;

/*********************
        SOLID
**********************/

robo.tileDfn.solid = function(){
	this.iconId = "tile-solid-icon";
	this.spriteId = "tile-solid";
	this.defaultAnimation = "idle";
	this.name = "Solid";
};

p = robo.tileDfn.solid.prototype = Object.create(robo.tileDfn.prototype);
p.constructor = robo.tileDfn.solid;

p.canStepOn = function(){
	return false;
};

p.getFieldEffects = function(){
	return {
		blockLaser: true
	};
};
/*********************
        PIT
**********************/

robo.tileDfn.pit = function(){
	this.iconId = "tile-pit-icon";
	this.spriteId = "tile-pit";
	this.defaultAnimation = "idle";
	this.name = "Pit";
};

p = robo.tileDfn.pit.prototype = Object.create(robo.tileDfn.prototype);
p.constructor = robo.tileDfn.pit;

p.onTouch = function(event){
	console.log("Touching Pit");
	event.level.currentAttempt.fail();
};

/*********************
        START
**********************/

robo.tileDfn.start = function(direction){
	this.iconId = "tile-start-icon";
	this.spriteId = "tile-start";
	this.defaultAnimation = "idle";
	this.direction = direction;
	this.name = "Start";
};

p = robo.tileDfn.start.prototype = Object.create(robo.tileDfn.prototype);
p.constructor = robo.tileDfn.start;

p.onTouch = function(event){
	console.log("Touching Avatar Start");
};


p.loadSettings = function(tile, settings){
	tile.state.direction = settings.direction;
};

p.exportSettings = function(tile){
	return {
		direction: tile.state.direction
	};
};


p.getEditSettings = function(tile){ 
	return [
		{
			name: 'direction',
			label: 'Facing Direction',
			value: tile.state.direction,
			type: 'direction'
		}
	];
};

/*********************
        END
**********************/

robo.tileDfn.end = function(){
	this.iconId = "tile-end-icon";
	this.spriteId = "tile-end";
	this.defaultAnimation = "idle";
	this.name = "End";
};

p = robo.tileDfn.end.prototype = Object.create(robo.tileDfn.prototype);
p.constructor = robo.tileDfn.end;

p.onTouch = function(event){
	console.log("Touching Avatar End", event);
	event.level.currentAttempt.win();
};

/*********************
        DOOR
**********************/

robo.tileDfn.door = function(){
	this.iconId = "tile-door-icon";
	this.spriteId = "tile-door";
	this.defaultAnimation = "opened";
	this.name = "Door";
};

p = robo.tileDfn.door.prototype = Object.create(robo.tileDfn.prototype);
p.constructor = robo.tileDfn.door;

p.canStepOn = function(tile){
	return !!tile.state.open; 
};

p.getFieldEffects = function(tile){
	return {
		blockLaser: !tile.state.open
	};
};

p.startRound = function(args, tile){
	var prev = tile.state.open;
	tile.state.open = tile.state.openBehaviour.getStateForRound(args.round);

	var animName = '';
	if(tile.state.open){ animName = prev? 'open' : 'opening'; }
	else{ animName = prev? 'closing': 'closed'; }

	var action = new robo.roundAction.animateSprite(tile.sprite, animName);
	action.setPriority(tile.state.open? 7 : 3);
	args.actions.push(action);
};

p.getDefaultSettings = function(){
	return {
		startOpen: true,
		openPattern: [true, false],
		openBehaviourType: 'pattern'
	};
}

p.loadSettings = function(tile, settings){
	var fullSettings = $.extend(true, {}, this.getDefaultSettings(), settings);
	tile.state.startOpen = tile.state.open = fullSettings.open;
	tile.state.pattern = fullSettings.openPattern;
	tile.state.openBehaviourType = fullSettings.openBehaviourType;
	tile.state.openBehaviour = new robo.roundBehaviour[fullSettings.openBehaviourType](tile.state, 'pattern');	
};

p.exportSettings = function(tile){
	return {
		open: tile.state.startOpen,
		openBehaviourType: tile.state.openBehaviourType,
		openPattern: tile.state.pattern
	};
};

p.connectSwitchObserver = function(tile, board){
	if(tile.state.openBehaviour){
		tile.state.openBehaviour.connectSwitchObserver(board.eventer);
	}
};

p.getEditSettings = function(tile){ 
	return [
		{
			name: 'startOpen',
			label: 'Start Open',
			value: tile.state.startOpen,
			type: 'bool'
		},
		{
			name: 'pattern',
			label: 'Open/Close Pattern',
			value: tile.state.openPattern,
			type: 'pattern',
			subtype: 'bool'
		},
		{
			name: 'openBehaviourType',
			label: 'Behaviour Type',
			value: tile.state.openBehaviourType,
			type: 'behaviour'
		}
	];
};


/*********************
        LASER
**********************/

robo.tileDfn.laser = function(){
	this.iconId = "tile-laser-icon";
	this.spriteId = "tile-laser";
	this.defaultAnimation = "activating";
	this.name = "Laser";
};

p = robo.tileDfn.laser.prototype = Object.create(robo.tileDfn.prototype);
p.constructor = robo.tileDfn.laser;

p.canStepOn = function(tile){
	return true;
};

p.createEffect = function(board, tile){
	var effect = new robo.fieldEffect.beam(board, tile);
	board.addEffect(effect);
	return effect;
}

p.startRound = function(args, tile){
	var prev = tile.state.active;
	if(!tile.effect){
		tile.effect = this.createEffect(args.board, tile);
	}

	tile.state.active = this.getOpenBehaviour(tile).getStateForRound(args.round);
	tile.state.direction = tile.state.directionBehaviour.getStateForRound(args.round);

	var action = new robo.roundAction.animateSprite(tile.sprite, tile.state.active ? 'activating': 'deactivating');
	action.setPriority(tile.state.open? 7 : 3);
	args.actions.push(action);

	tile.effect.setAttr({ active: tile.state.active, direction: tile.state.direction});
	var laserAction = new robo.roundAction.beam(tile.effect);
	args.actions.push(laserAction);
};

p.getOpenBehaviour = function(tile){
	return tile.state.openBehaviour  = tile.state.openBehaviour || new robo.roundBehaviour.alternate(tile.state, 'active'); 
}

p.getDefaultSettings = function(){
	return {
		directionPattern: [0,1,2,3],
		direction: 0,
		directionBehaviourType: 'pattern',
		active: false,
		openPattern: [true, false],
		openBehaviourType: 'pattern',
		openChannel: 0
	};
};

p.loadSettings = function(tile, settings){
	settings = $.extend(true, {}, this.getDefaultSettings(), settings || {});

	tile.state.initialDirection = tile.state.direction = settings.direction;
	tile.state.directionPattern = settings.directionPattern;
	tile.state.directionBehaviourType = settings.directionBehaviourType
	tile.state.directionBehaviour = new robo.roundBehaviour[settings.directionBehaviourType](tile.state, 'directionPattern');
	
	tile.state.startActive = tile.state.active = settings.active;	
	tile.state.openPattern = settings.openPattern;
	tile.state.openBehaviourType = settings.openBehaviourType;
	tile.state.openChannel = settings.openChannel;

	var behaviourKey = settings.openBehaviourType == "switch" ? 'active' : 'openPattern';
	tile.state.openBehaviour = new robo.roundBehaviour[settings.openBehaviourType](tile.state, behaviourKey, tile.state.openChannel);
};

p.exportSettings = function(tile){
	return {
		direction: tile.state.initialDiraction,
		directionPattern: tile.state.directionPattern,
		directionBehaviourType: tile.state.directionBehaviourType,

		active: tile.state.startActive,
		openPattern: tile.state.openPattern,
		openBehaviourType: tile.state.openBehaviourType,
		openChannel: tile.state.openChannel
	};
};

p.getFieldEffects = function(tile){
	return {
		blockLaser: !tile.state.active
	};
};


p.getEditSettings = function(tile){ 
	return [
		{
			name: 'direction',
			label: 'Initial Direction',
			value: tile.state.initialDirection,
			type: 'direction'
		},
		{
			name: 'directionPattern',
			label: 'Direction Pattern',
			value: tile.state.directionPattern,
			type: 'pattern',
			subtype: 'direction'
		},
		{
			name: 'directionBehaviourType',
			label: 'Direction Behaviour Type',
			value: tile.state.directionBehaviourType,
			type: 'behaviour'
		},
		{
			name: 'startActive',
			label: 'Start Active',
			value: tile.state.startActive,
			type: 'bool'
		},
		{
			name: 'openBehaviourType',
			label: 'Behaviour Type',
			value: tile.state.openBehaviourType,
			type: 'behaviour'
		},
		{
			name: 'openPattern',
			label: 'Open/Close Pattern',
			value: tile.state.openPattern,
			type: 'pattern',
			subtype: 'bool'
		},
		{
			name: 'openChannel',
			label: 'Open/Close Switch Channel',
			value: tile.state.openChannel,
			type: 'channel'
		}
	];
};

p.connectSwitchObserver = function(tile, board){
	if(tile.state.openBehaviour){
		tile.state.openBehaviour.connectSwitchObserver(board.eventer);
	}
};

/*********************
        SWITCH
**********************/

robo.tileDfn.switch = function(){
	this.iconId = "tile-switch-icon";
	this.spriteId = "tile-switch";
	this.defaultAnimation = "on";
	this.name = "Switch";
};

p = robo.tileDfn.switch.prototype = Object.create(robo.tileDfn.prototype);
p.constructor = robo.tileDfn.switch;

p.canStepOn = function(tile){
	return true;
};

p.startRound = function(args, tile){};

p.getDefaultSettings = function(){
	return {
		active: false,
		channel: 0
	};
};

p.loadSettings = function(tile, settings){
	settings = $.extend(true, {}, this.getDefaultSettings(), settings);
	tile.state.startActive = tile.state.active = settings.active;
	tile.state.channel = settings.channel || 0;
};

p.exportSettings = function(tile){
	return {
		active: tile.state.startActive,
		channel: tile.state.channel,
	};
};

p.getEditSettings = function(tile){ 
	return [
		{
			name: 'startActive',
			label: 'Start Active',
			value: tile.state.startActive,
			type: 'bool'
		},
		{
			name: 'channel',
			label: 'Channel',
			value: tile.state.channel,
			type: 'channel'
		},
	];
};

p.onTouch = function(event){
	console.log("Activating Switch");
	event.board.eventer.trigger('switch', {
		tile: event.tile, 
		on: event.tile.state.active, 
		channel: event.tile.state.channel
	});
};
window.robo = window.robo || {};
window.robo.util = window.robo.util || {};
var p;//prototype shortcut

robo.tileInspector = function() {
	this.initialize();
}

p = robo.tileInspector.prototype = new createjs.Container();

p.Container_initialize = p.initialize;
p.initialize = function() {
	this.Container_initialize();
	this.iconId = 'tile-inspect-icon';
	this.icon = null;
	// add custom setup logic here.
}

p.setIcon = function(library){
	if(this.icon){
		this.removeChild(this.icon);
	}
	this.icon = library.get("icon", this.iconId).clone();
	this.addChild(this.icon);
}
window.robo = window.robo || {};
window.robo.util = window.robo.util || {};

/****************************
			TILE
****************************/

robo.tile = function(tileDfn, x, y){
	var self = this;
	self.displayObject = null;
	self.coord = {x: x, y: y};
	self.tileDfn = tileDfn;
	self.state = {};
	self.attachedCard = null;
	return self;
};

robo.tile.prototype.update = function(event){
	this.tileDfn.update();
};

robo.tile.prototype.startRound = function(args){
	this.tileDfn.startRound(args, this);

	var tile = this;
	args.level.board.avatars.forEach(function(avatar, index){
		if(tile.coord.x == avatar.coord.x && tile.coord.y == avatar.coord.y){//Same position
			if(tile.attachedCard){
				tile.attachedCard.definition.action(avatar, tile, args.level.board);
			}
			else{
				tile.tileDfn.onTouch({
					avatar: avatar, 
					tile: tile, 
					board: args.level.board,
					level: args.level
				});
			}
		}
	
		//if has effect
		var effect = tile.getEffect();
		if(effect){
			//if avatar in effect field, affect it
			effect.tryAffect(avatar);
		}
	
	});
};

robo.tile.prototype.endRound = function(args){
	this.tileDfn.endRound(args, this);
};

robo.tile.prototype.getEffect = function(){
	return this.tileDfn.getEffect(this);
}

robo.tile.prototype.attachCard = function(card){
	this.attachedCard = card;
	this.attachedCardDO = card.definition.icon.clone();
	this.getDisplayObject().addChild(this.attachedCardDO);
};

robo.tile.prototype.unattachCard = function(card){
	this.getDisplayObject().removeChild(this.attachedCardDO);
	this.attachedCardDO = null;
	this.attachedCard = null;
};

robo.tile.prototype.getDisplayObject = function(){
	var self = this;
	if(self.displayObject == null){
		self.displayObject = new createjs.Container();
		self.tileDfn.render(this);
		self.attachBehaviour(self.displayObject);
	};
	return self.displayObject;
};

robo.tile.prototype.attachBehaviour = function(tileDO){
	var tile = this;
	tileDO.addEventListener('click', function(event){
		//TODO: Parental Heirarchy
		engine.level.phases[engine.level.currentPhase].onEvent('selectTile', {
			tile: tile
		});
	});
};

robo.tile.prototype.canStepOn = function(){
	return this.tileDfn.canStepOn(this);
};

robo.tile.prototype.loadSettings = function(settings){
	this.tileDfn.loadSettings(this, settings);
	return this;
}

robo.tile.prototype.export = function(){
	return {
		name: _.findKey(robo.util.tileDfns, this.tileDfn),
		settings: this.tileDfn.exportSettings(this)
	};
}

robo.tile.prototype.connectSwitchObservers = function(board){
	this.tileDfn.connectSwitchObserver(this, board);
};

robo.tile.prototype.getFieldEffects = function(){
 var dfn = this.tileDfn.getFieldEffects(this);
 var chip = {};
 if(this.attachedCard){
 	chip = this.attachedCard.getFieldEffects(this);
 }
 return {
 	blockLaser : chip.blockLaser || dfn.blockLaser
 };
};
window.robo = window.robo || {};
window.robo.util = window.robo.util || {};

robo.tileHand = function(level){
	this.tileDfns = [];
	this.level = level;
}

robo.tileHand.prototype.getLevel = function(level){
	return this.level;
};

robo.tileHand.prototype.fillHand = function(tileDfns){
	var self = this; 
	tileDfns.forEach(function(tileDfn){
		self.addTileDfn(tileDfn);
	});
}

robo.tileHand.prototype.emptyHand = function(tileDfns){
	var self = this; 
	this.getDisplayObject().removeAllChildren();
	this.tileDfns = [];
}

robo.tileHand.prototype.addTileDfn = function(tileDfn){
	var self = this;
	this.tileDfns.push(tileDfn);

	var tileDO = tileDfn.icon.clone();
	this.getDisplayObject().addChild(tileDO);
	var index = this.tileDfns.length - 1;
	tileDO.x = 60*(index % 4);
	tileDO.y = 60* (Math.floor(index/4));

	 //Add touch events
	 tileDO.addEventListener('rollover', function(event){
	 	//grow slightly
	 	tileDO.scaleX = tileDO.scaleY = 1.1;
	 });
	 tileDO.addEventListener('rollout', function(event){
	 	// return to normal size
	 	tileDO.scaleX = tileDO.scaleY = 1.0
	 });
	 tileDO.addEventListener('mousedown', function(event){
	 	self.drag(event, tileDfn, tileDO);
	 });
	 tileDO.addEventListener('pressup', function(event){
	 	self.drop(event, tileDfn, tileDO);
	 });
	 tileDO.addEventListener('pressmove', function(event){
	 	self.dragMove(event, tileDfn, tileDO);
	 });
};

robo.tileHand.prototype.drag = function(event, tileDfn, tileDO){
	this.state = this.state || {};
	this.state.dragging = tileDO;
	this.state.dragOffset = {
		x: tileDO.x - event.stageX,
		y: tileDO.y - event.stageY
	};
}

robo.tileHand.prototype.drop = function(event, tileDfn, tileDO){
	this.state = this.state || {};
	if(this.state.dragging == tileDO){
		this.state.dragging = null;
		var b = tileDO.getBounds();
		var tile = this.getLevel().dropTileAtLocal(tileDO, {x: b.width/2, y:b.height/2});
		if(tile){
			this.getLevel().placeTile(tileDfn, tile);
		}
	}
}

robo.tileHand.prototype.dragMove = function(event, tileDfn, tileDO){
	this.state = this.state || {};
	if(this.state.dragging == tileDO){
		tileDO.x = event.stageX + this.state.dragOffset.x;
		tileDO.y = event.stageY + this.state.dragOffset.y;
	}
}

robo.tileHand.prototype.getDisplayObject = function(){
	var self = this;
	if(!this.displayObject){
		this.displayObject = new createjs.Container();
	}

	return this.displayObject;
};

robo.tileHand.prototype.update = function(){
	this.tileDns.forEach(function(tileDfn){
		tileDfn.update();
	});
}

// tool
	//eventer -> on change, on use, etc

// level edit tool
	//Inspector
	//StampTile

// place chips tool

	//Inspector
	//StampChip

window.robo = window.robo || {};
window.robo.util = window.robo.util || {};
var p;//prototype shortcut

robo.tool = function(){
	this.active = false;
	this.name = "No Tool";
	this.bleep = 'bloop';
}

p = robo.tool.prototype;

p.activate = function(){
	this.active = true;
};
p.deactivate = function(){
	this.active = true;
};

p.select = function(type, item, options){};


robo.tools = {};

robo.tools.inspect = function(){
	this.active = false;
	this.name = "Inspect";
};

p = robo.tools.inspect.prototype = Object.create(robo.tool.prototype);

p.select = function(type, item, options){
	var dialog = new robo.dialogs.inspectDialog();
	dialog.open(type, item, function(settings){
		if(type == "tile"){
			console.log("Applying settings to tile");
			item.tileDfn.loadSettings(item, settings);
		}
	});
};
robo.util = robo.util || {};

robo.util.readAttribute = function(target, propertyChain){
	target = ko.utils.unwrapObservable(target);

	if (target == null || target == "undefined") {
		return target;
	}

	// Check if the property is as-specified
	if (target[propertyChain[0]]) {
		target = target[propertyChain[0]];
	}
	else {
		target = undefined;
	}

	propertyChain = propertyChain.slice(1);

	if (propertyChain.length > 0) {
		return robo.util.readAttribute(target, propertyChain);
	}
	else {
		return target;
	}	
}

robo.util.dotRead = function(target, dotNotation){
	try {
		return ko.unwrap(robo.util.readAttribute(target, dotNotation.split(".")));
	}
	catch(ex){
		console.trace();
		throw new Error(ex);
	}
}

robo.util.toggleObs = function(obs){
	obs(!obs());
};

robo.util.setObs = function(obs, value){
	return function(){
		obs(value);
	};
};

robo.util.pattern = {};

robo.util.pattern.add = function(obs, type, defaultValue){
	return function(){
		obs.push({
			type: type,
			value: ko.observable(defaultValue)
		});
	};
};

robo.util.pattern.remove = function(obs){
	return function(){
		if(obs().length > 1){				
			obs.pop();
		}
	};
};
ko.bindingHandlers.direction = {
	init: function(element, valueAccessor, allBindings, viewModel, bindingContext){
		
	},
	update: function(element, valueAccessor, allBindings, viewModel, bindingContext){

	}
};
robo.dialogs = robo.dialogs || {};

robo.dialogs.inspectDialog = function(){
	this.init();
	this.template = 'dialog-inspect';
};

p = robo.dialogs.inspectDialog.prototype = Object.create(robo.dialog.prototype);
p.constructor = robo.dialogs.inspectDialog;

p.mapSetting = function(setting){
	var obj = {
		name: setting.name,
		type: setting.type,
		subtype: setting.subtype,
		label: setting.label,		
	};

	if(setting.type == "pattern"){
		obj.value = ko.observableArray(_.map(setting.value, function(value){
			return {
				value: ko.observable(value),
				type: obj.subtype
			};
		}));
	}
	else{
		obj.value = ko.observable(setting.value);
	}

	return obj;
}

p.readSettingValue = function(setting){
	if(setting.type == "pattern"){
		return _.map(ko.unwrap(setting.value), function(item){
			return ko.unwrap(item.value);
		});
	}
	else{
		return ko.unwrap(setting.value);
	}
};

p.onOpen = function(type, item, callback){
	window.item = item;
	this.callback = callback;
	this.type = ko.observable(type);
	this.item = ko.observable(item);
	this.settings = null;
	
	if(this.type() == "tile"){
		this.settings = _.map(this.item().tileDfn.getEditSettings(this.item()), this.mapSetting);
	}

	window.settings = this.settings;
};

p.save = function(){
	var data = {};

	_.each(this.settings, function(item, key){
		data[item.name] = this.readSettingValue(item);
	}.bind(this));

	if(_.isFunction(this.callback)){
		this.callback(data);
	}

	this.close();
};