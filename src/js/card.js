window.robo = window.robo || {};
window.robo.util = window.robo.util || {};

/****************************
		Card
****************************/
robo.cardDfn = function(id, action, icon, color){
	this.icon = icon;
	this.action = action;
	this.id = id;
	this.color = color;

	return this;
}


robo.card = function(cardDfn, hand){
	var self = this;
	this.hand = hand;
	this.tapped = false;
	this.definition = cardDfn;
}

robo.card.prototype.getHand = function(){
	return this.hand;
}

robo.card.prototype.attachTo = function(tile){
	if(!this.tapped){
		this.tapped = true;
		tile.attachCard(this);
	}
}

robo.card.prototype.getDisplayObject = function(){
	var self = this;
	self.state = self.state || {};
	if(!self.displayObject){
		self.displayObject = new createjs.Container();
		//Create card backing
		var graphics = new createjs.Graphics();
		var backing = new createjs.Shape(graphics);
		backing.graphics
			.beginStroke("#000")
			.beginFill("#fff")
			.drawRoundRect(0,0,50,50,5);
		self.displayObject.addChild(backing);
		 //Create icon
		var icon = self.definition.icon.clone(), 
		iconBounds = icon.getBounds();
		
		icon.x = 25 - iconBounds.width/2;
		icon.y = 25 - iconBounds.height/2;
		self.displayObject.addChild(icon);
		
		 // Create count
		 // self.displayObject
		
		 //Add touch events
		 self.displayObject.addEventListener('rollover', function(event){
		 	//grow slightly
		 	self.displayObject.scaleX = self.displayObject.scaleY =1.1;
		 });
		 self.displayObject.addEventListener('rollout', function(event){
		 	// return to normal size
		 	self.displayObject.scaleX = self.displayObject.scaleY = 1.0
		 });
		 self.displayObject.addEventListener('mousedown', function(event){
		 	self.drag(event);
		 });
		 self.displayObject.addEventListener('pressup', function(event){
		 	self.drop(event);
		 });
		 self.displayObject.addEventListener('pressmove', function(event){
		 	self.dragMove(event);
		 });
	}

	return this.displayObject;
}

robo.card.prototype.getFieldEffects = function(tile){
	return {
		blockLaser: false
	};
};

robo.card.prototype.update = function(){
	this.getDisplayObject().alpha = this.tapped ? 0 : 100;
};

robo.card.prototype.drag = function(event){
	var self = this;
	self.state = self.state||{};
	self.state.drag = true;
	self.state.dragOffset = {
		x: self.displayObject.x - event.stageX,
		y: self.displayObject.y - event.stageY
	};
}

robo.card.prototype.drop = function(event){
	var self = this;
	self.state = self.state||{};
	if(self.state.drag){
		self.state.drag = false;
		var b = self.displayObject.getBounds();

		var tile = self.getHand().getLevel().dropCardAtLocal(self, {x: b.width/2, y:b.height/2});
		if(tile){
			self.getHand().getLevel().playCard(self, tile);
		}
	}
}
robo.card.prototype.dragMove = function(event){
	var self = this;
	self.state = self.state||{};
	if(self.state.drag){
		self.displayObject.x = event.stageX + self.state.dragOffset.x;
		self.displayObject.y = event.stageY+ self.state.dragOffset.y;
	}
}



/****************************
		Card Definitions
****************************/

	robo.util.cardDfns = {};