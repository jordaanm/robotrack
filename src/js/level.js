window.robo = window.robo || {};
window.robo.util = window.robo.util || {};

/****************************
		Level
****************************/

	robo.level = function(board, cardDfns){
		// Setup board
		this.board = null;
		this.round = 0;

		//Setup hand
		this.hand = new robo.hand(this);
		this.cardDfns = null;

		this.editorHand = new robo.tileHand(this);

		this.simStarted = false;

		this.phases = [
			robo.util.phases.loadLevel,
			robo.util.phases.playChips,
			robo.util.phases.runSimulation
		];


		this.currentPhase = 0;

		this.currentAttempt = null;
		this.previousAttempts = [];

		// this.init();
	};

	robo.level.prototype.init = function(){
		this.currentAttempt = new robo.attempt(this);
		this.phases[this.currentPhase].onEvent("startPhase", {level: this});
		return this;
	};

	robo.level.prototype.loadCards = function(cardDfnsJson){
		this.cardDfns = _.map(cardDfnsJson, function(name){
			return robo.util.cardDfns[name];
		});
		return this;
	};

	
	robo.level.prototype.loadBoard = function(boardJson){
		this.board = robo.util.loadMapJson(boardJson);
		return this;
	};

	robo.level.prototype.exportBoard = function(){
		return this.board.exportMap();
	};

	robo.level.prototype.exportCards = function(){
		return _.map(this.cardDfns, function(dfn){
			return _.findKey(robo.util.cardDfns, dfn);
		});
	};

	robo.level.prototype.startAttempt = function(){
		if(this.currentAttempt){
			this.previousAttempts.push(currentAttempt);
		}
		this.currentAttempt = new robo.attempt(this);
	};

	robo.level.prototype.dropCardAtLocal = function(card, local){
		return this.board.dropCardAtLocal(card, local);
	};

	robo.level.prototype.dropTileAtLocal = function(tileDO, local){
		return this.board.dropTileAtLocal(tileDO, local);
	};

	robo.level.prototype.playCard = function(card, tile){
		return this.phases[this.currentPhase].onEvent('playCard', {
			level: this,
			card: card,
			tile: tile
		});
	};

	robo.level.prototype.placeTile = function(tileDfn, tile){
		return this.phases[this.currentPhase].onEvent('placeTile', {
			level: this,
			tileDfn: tileDfn,
			tile: tile
		});
	};

	robo.level.prototype.selectTile = function(tileDfn, tile){
		return this.phases[this.currentPhase].onEvent('selectTile', {
			level: this,
			tileDfn: tileDfn,
			tile: tile
		});
	};

	robo.level.prototype.nextPhase = function(){
		if(this.phases.length > (this.currentPhase +1)){

			this.phases[this.currentPhase].onEvent("endPhase", {level: this});
			this.currentPhase++;
			this.phases[this.currentPhase].onEvent("startPhase", {level:this});
		}
	}

	robo.level.prototype.getDisplayObject = function(){
		var self = this;
		if(!this.displayObject){
			this.displayObject = new createjs.Container();
			var b = this.board.getDisplayObject();
			var h = this.hand.getDisplayObject();
			var e = this.editorHand.getDisplayObject();
			h.x = 550;
			h.y = 250;
			e.x = 550;
			e.y = 250;
			this.displayObject.addChild(b);
			this.displayObject.addChild(h);
			this.displayObject.addChild(e);

			this.displayObject.addEventListener("tick", function(event){
				self.update.call(self, event.params[0]);
			} );

		}

		return this.displayObject;
	}

	robo.level.prototype.showEditorCards = function(){
		var dfns = [];
		for(name in robo.util.tileDfns){
			dfns.push(robo.util.tileDfns[name]);
		}
		this.editorHand.fillHand(dfns);
	};

	robo.level.prototype.hideEditorCards = function(){
		this.editorHand.emptyHand();
	};

	robo.level.prototype.update = function(timer){
		var self = this;
		window.timer = timer;
		this.phases[this.currentPhase].update(self, timer);
		
		self.board.update(timer);
		self.hand.update(timer);
	}

	robo.level.prototype.getIncompleteActions = function(){
		if(this.currentAttempt){
			return this.currentAttempt.getIncompleteActions();
		}
		else{
			return [];
		}
	};

	robo.level.prototype.startRound = function(){
		this.currentAttempt.startRound();

		this.board.startRound({
			level: this,
			round: this.currentAttempt.roundCount,
			actions: this.currentAttempt.getActions()
		});
	};	

	robo.level.prototype.endRound = function(){
		this.currentAttempt.endRound();
		this.board.endRound({
			round: this.currentAttempt.roundCount,
			actions: this.currentAttempt.getActions()
		});
	};
