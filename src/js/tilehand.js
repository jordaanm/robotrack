window.robo = window.robo || {};
window.robo.util = window.robo.util || {};

robo.tileHand = function(level){
	this.tileDfns = [];
	this.level = level;
}

robo.tileHand.prototype.getLevel = function(level){
	return this.level;
};

robo.tileHand.prototype.fillHand = function(tileDfns){
	var self = this; 
	tileDfns.forEach(function(tileDfn){
		self.addTileDfn(tileDfn);
	});
}

robo.tileHand.prototype.emptyHand = function(tileDfns){
	var self = this; 
	this.getDisplayObject().removeAllChildren();
	this.tileDfns = [];
}

robo.tileHand.prototype.addTileDfn = function(tileDfn){
	var self = this;
	this.tileDfns.push(tileDfn);

	var tileDO = tileDfn.icon.clone();
	this.getDisplayObject().addChild(tileDO);
	var index = this.tileDfns.length - 1;
	tileDO.x = 60*(index % 4);
	tileDO.y = 60* (Math.floor(index/4));

	 //Add touch events
	 tileDO.addEventListener('rollover', function(event){
	 	//grow slightly
	 	tileDO.scaleX = tileDO.scaleY = 1.1;
	 });
	 tileDO.addEventListener('rollout', function(event){
	 	// return to normal size
	 	tileDO.scaleX = tileDO.scaleY = 1.0
	 });
	 tileDO.addEventListener('mousedown', function(event){
	 	self.drag(event, tileDfn, tileDO);
	 });
	 tileDO.addEventListener('pressup', function(event){
	 	self.drop(event, tileDfn, tileDO);
	 });
	 tileDO.addEventListener('pressmove', function(event){
	 	self.dragMove(event, tileDfn, tileDO);
	 });
};

robo.tileHand.prototype.drag = function(event, tileDfn, tileDO){
	this.state = this.state || {};
	this.state.dragging = tileDO;
	this.state.dragOffset = {
		x: tileDO.x - event.stageX,
		y: tileDO.y - event.stageY
	};
}

robo.tileHand.prototype.drop = function(event, tileDfn, tileDO){
	this.state = this.state || {};
	if(this.state.dragging == tileDO){
		this.state.dragging = null;
		var b = tileDO.getBounds();
		var tile = this.getLevel().dropTileAtLocal(tileDO, {x: b.width/2, y:b.height/2});
		if(tile){
			this.getLevel().placeTile(tileDfn, tile);
		}
	}
}

robo.tileHand.prototype.dragMove = function(event, tileDfn, tileDO){
	this.state = this.state || {};
	if(this.state.dragging == tileDO){
		tileDO.x = event.stageX + this.state.dragOffset.x;
		tileDO.y = event.stageY + this.state.dragOffset.y;
	}
}

robo.tileHand.prototype.getDisplayObject = function(){
	var self = this;
	if(!this.displayObject){
		this.displayObject = new createjs.Container();
	}

	return this.displayObject;
};

robo.tileHand.prototype.update = function(){
	this.tileDns.forEach(function(tileDfn){
		tileDfn.update();
	});
}
