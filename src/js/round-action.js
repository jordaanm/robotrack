window.robo = window.robo || {};
window.robo.util = window.robo.util || {};
var p;//prototype shortcut

robo.roundAction = function(){
	this.bComplete = false;
	this.queue = null;
	this.priority = 5;
};

p = robo.roundAction.prototype;

p.complete = function(){
	this.bComplete = true;
};

p.update = function(){};

p.isComplete = function(){
	return !!this.bComplete;
};

p.setPriority = function(newPriority){
	this.priority = newPriority;
};

robo.roundAction.animatePosition = function(displayObject, targetPos, baseDuration){
	this.animProgress = 0;
	this.baseDuration = baseDuration;
	this.displayObject = displayObject;
	this.targetPos = targetPos;
	this.origPos = {x: this.displayObject.x, y: this.displayObject.y};
};

p = robo.roundAction.animatePosition.prototype = Object.create(robo.roundAction.prototype);

p.update = function(deltaTime){

	this.animProgress += deltaTime;//todo: get playback rate, multiply;
	
	var progress = this.animProgress/this.baseDuration;
	if(progress < 1.0){
		this.displayObject.x = (this.origPos.x + ((this.targetPos.x - this.origPos.x) * progress));
		this.displayObject.y = (this.origPos.y + ((this.targetPos.y - this.origPos.y) * progress));
	}
	else{
		this.complete();
	}
};

robo.roundAction.animateSprite = function(sprite, animation, framerateOverride){
	this.sprite = sprite;
	sprite.gotoAndPlay(animation);

	if(framerateOverride){
		this.originalFramerate = sprite.framerate;
		sprite.framerate = framerateOverride;
	}

	this.sprite.on("animationend", this.complete.bind(this));
};

p = robo.roundAction.animateSprite.prototype =  Object.create(robo.roundAction.prototype);
p.constructor = robo.roundAction.animateSprite;

p.complete = function(){
	this.bComplete = true;
	if(this.sprite){
		if(this.originalFramerate){
			this.sprite.framerate = this.originalFramerate;
		}
		this.sprite.off("animationend", this.complete);
		this.sprite = null;	
	}
};

robo.roundAction.beam = function(effect){
	this.effect = effect;
	this.priority = 2;
};


p = robo.roundAction.beam.prototype =  Object.create(robo.roundAction.prototype);
p.constructor = robo.roundAction.beam;

p.update = function(deltaTime){
	this.effect.animateBeam(this.complete.bind(this));
}