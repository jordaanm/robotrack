window.robo = window.robo || {};
window.robo.util = window.robo.util || {};

robo.fieldEffect = function(){

};

robo.fieldEffect.prototype.startRound = function(){};
robo.fieldEffect.prototype.endRound = function(){};

robo.fieldEffect.prototype.tryAffect = function(avatar){
	if(this.inAffectedArea(avatar.coord.x, avatar.coord.y)){
		this.applyEffect(avatar);
	}
};

robo.fieldEffect.prototype.applyEffect = function(avatar){
	avatar.die();
};

robo.fieldEffect.prototype.inAffectedArea = function(x, y){
	return false;
};

robo.fieldEffect.beam = function(board, tile){
	this.board = board;
	this.tile = tile;
	this.active = false;
	this.direction = 0;
};

p = robo.fieldEffect.beam.prototype = Object.create(robo.fieldEffect.prototype);
p.constructor = robo.fieldEffect.beam;

p.setAttr = function(attr){
	this.prevDirection = this.direction;
	this.prevActive = this.active;

	this.direction = attr.direction;
	this.active = attr.active;
}

p.animateBeam = function(callback){
	var shape = this.getDisplayObject(true);
	if(_.isFunction(callback)){
		callback();
	}
};

p.getEndCoords = function(){
		var direction = {
			x: Math.sin(this.direction*Math.PI/2),
			y: Math.cos(this.direction*Math.PI/2)
		};

		var scanResults = this.board.scanTilesForField(this.tile.coord.x, this.tile.coord.y, this.direction, 'blockLaser');

		return {
			x: scanResults.x,
			y: scanResults.y
		};
	};

p.getDisplayObject = function(forceUpdate){
	if(!this.shape || forceUpdate){
		this.shape = this.shape || new createjs.Shape();

		if(forceUpdate){ this.shape.graphics.clear(); }

		var pos = this.board.getTileCenter(this.tile.coord.x, this.tile.coord.y),
		endCoords = this.getEndCoords(),
		endPos = this.board.getTileCenter(endCoords.x, endCoords.y);

		this.shape.graphics.beginStroke("#ff0000").moveTo(pos.x,pos.y).lineTo(endPos.x,endPos.y);
	}
	this.shape.visible = !!this.active;

	return this.shape;
};

p.startRound = function(args){
	console.log("EFFECT START", args);
};

p.endRound = function(args){
	//If robot in our field,  kill it
	console.log("EFFECT END", args);
	// if()
};

p.inAffectedArea = function(x, y){
	if(!this.active) { return false; }
	var endCoords = this.getEndCoords(),
	startCoords = this.tile.coord,
	minX = Math.min(startCoords.x, endCoords.x),
	maxX = Math.max(startCoords.x, endCoords.x),
	minY = Math.min(startCoords.y, endCoords.y),
	maxY = Math.max(startCoords.y, endCoords.y);


	switch(this.direction % 4){
		case 0:
		case 2:
			return y == startCoords.y && y >= minY && y <= maxY;
		case 1:
		case 3:
			return x == startCoords.x && y >= minY && y <= maxY;
		default:
			return false;
	}
};