window.robo = window.robo || {};
window.robo.util = window.robo.util || {};

/****************************
			TILE
****************************/

robo.tile = function(tileDfn, x, y){
	var self = this;
	self.displayObject = null;
	self.coord = {x: x, y: y};
	self.tileDfn = tileDfn;
	self.state = {};
	self.attachedCard = null;
	return self;
};

robo.tile.prototype.update = function(event){
	this.tileDfn.update();
};

robo.tile.prototype.startRound = function(args){
	this.tileDfn.startRound(args, this);

	var tile = this;
	args.level.board.avatars.forEach(function(avatar, index){
		if(tile.coord.x == avatar.coord.x && tile.coord.y == avatar.coord.y){//Same position
			if(tile.attachedCard){
				tile.attachedCard.definition.action(avatar, tile, args.level.board);
			}
			else{
				tile.tileDfn.onTouch({
					avatar: avatar, 
					tile: tile, 
					board: args.level.board,
					level: args.level
				});
			}
		}
	
		//if has effect
		var effect = tile.getEffect();
		if(effect){
			//if avatar in effect field, affect it
			effect.tryAffect(avatar);
		}
	
	});
};

robo.tile.prototype.endRound = function(args){
	this.tileDfn.endRound(args, this);
};

robo.tile.prototype.getEffect = function(){
	return this.tileDfn.getEffect(this);
}

robo.tile.prototype.attachCard = function(card){
	this.attachedCard = card;
	this.attachedCardDO = card.definition.icon.clone();
	this.getDisplayObject().addChild(this.attachedCardDO);
};

robo.tile.prototype.unattachCard = function(card){
	this.getDisplayObject().removeChild(this.attachedCardDO);
	this.attachedCardDO = null;
	this.attachedCard = null;
};

robo.tile.prototype.getDisplayObject = function(){
	var self = this;
	if(self.displayObject == null){
		self.displayObject = new createjs.Container();
		self.tileDfn.render(this);
		self.attachBehaviour(self.displayObject);
	};
	return self.displayObject;
};

robo.tile.prototype.attachBehaviour = function(tileDO){
	var tile = this;
	tileDO.addEventListener('click', function(event){
		//TODO: Parental Heirarchy
		engine.level.phases[engine.level.currentPhase].onEvent('selectTile', {
			tile: tile
		});
	});
};

robo.tile.prototype.canStepOn = function(){
	return this.tileDfn.canStepOn(this);
};

robo.tile.prototype.loadSettings = function(settings){
	this.tileDfn.loadSettings(this, settings);
	return this;
}

robo.tile.prototype.export = function(){
	return {
		name: _.findKey(robo.util.tileDfns, this.tileDfn),
		settings: this.tileDfn.exportSettings(this)
	};
}

robo.tile.prototype.connectSwitchObservers = function(board){
	this.tileDfn.connectSwitchObserver(this, board);
};

robo.tile.prototype.getFieldEffects = function(){
 var dfn = this.tileDfn.getFieldEffects(this);
 var chip = {};
 if(this.attachedCard){
 	chip = this.attachedCard.getFieldEffects(this);
 }
 return {
 	blockLaser : chip.blockLaser || dfn.blockLaser
 };
};