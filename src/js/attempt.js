window.robo = window.robo || {};
window.robo.util = window.robo.util || {};

/***************************
		Attempt
***************************/
	robo.attempt = function(level){
		this.level = level;
		this.state = 'idle';
		this.roundCount = 0;
		this.actionQueue = [];
	};

	robo.attempt.prototype.fail = function(){
		if(this.state != "fail"){
			this.state = 'fail';
			console.log("YOU FAIL");
		}
	};

	robo.attempt.prototype.win = function(){
		if(this.state != "win"){
			this.state = 'win';
			console.log("YOU WIN");			
		}
	};

	robo.attempt.prototype.getActions = function(){
		return this.actionQueue || [];
	}

	robo.attempt.prototype.getIncompleteActions = function(){
		var allActions = this.getActions();
		return _.filter(allActions, function(action){
			if(!_.isObject(action)) { return false; }
			return !action.isComplete();
		});
	}

	robo.attempt.prototype.startRound = function(){
		this.actionQueue = [];
		this.roundCount++;
	};

	robo.attempt.prototype.endRound = function(){
	};

