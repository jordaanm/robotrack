window.robo = window.robo || {};
window.robo.util = window.robo.util || {};
var p;

robo.util.observer = function(){
	this.events = {};
};

p = robo.util.observer.prototype;

p.subscribe = function(eventName, cb, data, context){
	this.getEvent(eventName).push({callback:cb, data: data, context: context});
	return this;
};

p.unsubscribe = function(eventName, callback){
	var index = _.chain(this.getEvent)
	.pluck('callback')
	.indexOf(callback)
	.value();
	this.getEvent(eventName).splice(index, 1);

	return this;
};

p.trigger = function(eventName, eventData){
	_.each(this.getEvent(eventName), function(sub, index){
		var finalData = $.extend(true, {data: sub.data}, eventData);
		sub.callback.call(sub.context, finalData);
	});
	return this;
};

p.getEvent = function(eventName){
	this.events[eventName] = this.events[eventName] || [];
	return this.events[eventName];
};