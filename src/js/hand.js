window.robo = window.robo || {};
window.robo.util = window.robo.util || {};

robo.hand = function(level){
	this.cards = [];
	this.level = level;
}


robo.hand.prototype.getLevel = function(level){
	return this.level;
};

robo.hand.prototype.fillHand = function(cardDfns){
	var self = this;
	cardDfns.forEach(function(cardDfn){
		self.addCard(new robo.card(cardDfn, self));
	});
};

robo.hand.prototype.emptyHand = function(){
	var self = this;
	this.cards.forEach(function(card){
		self.removeCard(card);
	})
}


robo.hand.prototype.addCard = function(card){
	this.cards.push(card);

	var cardDO = card.getDisplayObject();
	this.getDisplayObject().addChild(cardDO);
	var index = this.cards.length - 1;
	cardDO.x = 60*(index % 4);
	cardDO.y = 60* (Math.floor(index/4));
};

robo.hand.prototype.removeCard = function(card){
	var index = this.cards.indexOf(card);
	if(index > -1){
		var cardDO = card.getDisplayObject();
		this.getDisplayObject().removeChild(cardDO);
		this.cards.splice(index, 1);
	}
}

robo.hand.prototype.getDisplayObject = function(){
	var self = this;
	if(!this.displayObject){
		this.displayObject = new createjs.Container();
	}

	return this.displayObject;
};

robo.hand.prototype.update = function(){
	// this.getDisplayObject().update();
	this.cards.forEach(function(card){
		card.update();
	});
}