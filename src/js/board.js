window.robo = window.robo || {};
window.robo.util = window.robo.util || {};

/****************************
		BOARD
****************************/
robo.board = function(width, height){

	this.width = width;
	this.height = height;
	
	this.tiles = [];
	this.defaultTileName = "vacant";

	this.effects = [];

	this.avatars = [];

	this.displayObject = null;
	this.tilesize = robo.settings.tilesize;
	this.tileGap = robo.settings.tileGap;
	
	this.eventer = new robo.util.observer();

	this.init(width, height);

	return this;
};

robo.board.prototype.getSettings = function(){
	return [
		{type: 'number', value: this.getDisplayObject().scaleX, id: 'scale', label: 'scale'},
		{type: 'number', value: this.width, id: 'width', label: 'width'},
		{type: 'number', value: this.height, id: 'height', label: 'height'}
	];
};

robo.board.prototype.applySettings = function(settings){
	var newWidth = settings.width;
	var newHeight = settings.height;
	if(newWidth != this.width || newHeight != this.height){
		this.setDimensions(newWidth, newHeight);
	}
	if(settings.scale){
		this.getDisplayObject().scaleX = this.getDisplayObject().scaleY = settings.scale;	
	}
};

robo.board.prototype.edit = function(){
	var dialog = new robo.dialogs.editBoardDialog();
	dialog.open(this, this.applySettings.bind(this));
};

robo.board.prototype.exportMap = function(){
	return {
		width: this.width,
		height: this.height,
		default: this.defaultTileName,
		tiles: _.map(this.tiles, function(tile){
		return tile.export();
		})
	};
};

robo.board.prototype.init = function(width, height){
	this.avatarContainer = new createjs.Container();
	this.tileContainer = new createjs.Container();
	this.effectContainer = new createjs.Container();

	this.avatarContainer.name = "Avatar Container";
	this.setDimensions(width, height);
	return this;
};

robo.board.prototype.update = function(deltaTime, timer){
	var self = this;
	//Update Tiles
	this.tiles.forEach(function(tile){
		tile.update(deltaTime, timer);
	});

	//Update Avatars
	this.avatars.forEach(function(avatar){
		avatar.update(deltaTime, timer);
	});
};

robo.board.prototype.startRound = function(args){

	// args = $.extend(true, { board: this}, args);
	args.board = this;

	this.tiles.forEach(function(tile){
		tile.startRound(args);
	});

	// this.effects.forEach(function(effect){
	// 	effect.startRound(args);
	// });

	this.avatars.forEach(function(avatar){
		avatar.startRound(args);
	});
};

robo.board.prototype.endRound = function(args){

	this.avatars.forEach(function(avatar){
		avatar.endRound(args);
	});

	// console.log("EEE", this.effects);
	// this.effects.forEach(function(effect){
	// 	effect.endRound(args);
	// });
	
	this.tiles.forEach(function(tile){
		tile.endRound(args);
	});
};

robo.board.prototype.dropCardAtLocal = function(card, local){
	var self = this;
	var retTile = null;
	self.tiles.forEach(function(tile){
		var pt = tile.getDisplayObject().localToLocal(25, 25, card.getDisplayObject());
		if(pt.y < 50 && pt.y > 0 && pt.x < 50 && pt.x > 0){
			retTile = tile;
		}
	});
	return retTile;
};

robo.board.prototype.dropTileAtLocal = function(tileDO, local){
	var self = this;
	var retTile = null;
	self.tiles.forEach(function(tile){
		var pt = tile.getDisplayObject().localToLocal(25, 25, tileDO);
		if(pt.y < 50 && pt.y > 0 && pt.x < 50 && pt.x > 0){
			retTile = tile;
		}
	});
	return retTile;
};

robo.board.prototype.getTile = function(x, y){
	if(x >= this.width || x < 0 || y >= this.height || y < 0){
		return null;
	}
	else{
		return this.tiles[this.width*y+x];
	}
};

robo.board.prototype.removeTile = function(x,y){
	var tile = this.tiles[this.getIndexFromCoord(x,y)];
	if(tile){
		this.getDisplayObject().removeChild(tile.getDisplayObject());
		this.tiles[this.getIndexFromCoord(x,y)] = null;
	}
};

robo.board.prototype.setTile = function(x,y,defn, settings){

	//Remove existing tile
	this.removeTile(x,y);
	var tile = new robo.tile(defn,x,y);
	this.tiles[this.getIndexFromCoord(x,y)] = tile;
	var dObj = tile.getDisplayObject();

	dObj.x = (this.tilesize+this.tileGap)*x;
	dObj.y = (this.tilesize+this.tileGap)*y;
	this.tileContainer.addChild(dObj);

	tile.loadSettings(settings);	

	this.getDisplayObject().setChildIndex(this.avatarContainer, this.getDisplayObject().children.length-2);
};

robo.board.prototype.setDimensions = function(width, height){
	this.emptyBoard();
	this.width = width;
	this.height = height;
	this.tiles = new Array(width * height);
	_.each(this.tiles, function(tile, index){
		var coord = this.getCoordFromIndex(index);
		this.setTile(coord.x, coord.y, robo.util.tileDfns[this.defaultTileName]); 
	}.bind(this));
};

robo.board.prototype.emptyBoard = function(){
	var self = this;
	this.tiles.forEach(function(tile, index){
		self.removeTile(self.getCoordFromIndex(index));
	});
};

robo.board.prototype.fillBoard = function(arrDefn){
	var self = this;
	arrDefn.forEach(function(data, index){
		var coord = self.getCoordFromIndex(index);
		self.setTile(coord.x, coord.y, data.definition, data.settings);
	});
};

robo.board.prototype.getIndexFromCoord = function(x, y){
	if(typeof x == "object"){
		y = x.y;
		x = x.x;
	}
	return this.width*y+x;
};

robo.board.prototype.getCoordFromIndex = function(i){
	y = ~~(i/this.width);
	x = i % this.width;
	return {x: x, y: y};
};

robo.board.prototype.buildDisplayObject = function(){
	displayObject = new createjs.Container();
	displayObject.name = "Board";
	displayObject.addChild(this.tileContainer);
	displayObject.addChild(this.avatarContainer);
	displayObject.addChild(this.effectContainer);
	return displayObject;
};

robo.board.prototype.getDisplayObject = function(){
	return (this.displayObject = this.displayObject || this.buildDisplayObject());
};

robo.board.prototype.getCoordBounds = function(x, y){
	return {
		top: y*(this.tilesize + this.tileGap),
		left: x*(this.tilesize + this.tileGap),
		width: this.tilesize,
		height: this.tilesize
	};
};

robo.board.prototype.getTileCenter = function(x, y){
	var bounds = this.getCoordBounds(x, y);
	return {
		x: bounds.left + bounds.width/2,
		y: bounds.top + bounds.height/2
	};
};

robo.board.prototype.addAvatar = function(avatar){
	this.avatars.push(avatar);
	avatar.setBoard(this);
	var dObj = avatar.getDisplayObject();
	this.resizeToFitTile(dObj);
	this.avatarContainer.addChild(dObj);
};

robo.board.prototype.resizeToFitTile = function(displayObject){
	var bounds = displayObject.getBounds();
	displayObject.scaleX = this.tilesize / bounds.width;
	displayObject.scaleY = this.tilesize / bounds.height;
};

robo.board.prototype.connectSwitchObservers = function(){
	_.each(this.tiles, function(tile){
		tile.connectSwitchObservers(this);
	}.bind(this));
};

robo.board.prototype.addEffect = function(effect){
	this.effects.push(effect);
	this.effectContainer.addChild(effect.getDisplayObject());

	return effect;
};

robo.board.prototype.removeEffect = function(effect){
	var i = this.effects.indexOf(effect);
	if(i > -1){
		this.effectContainer.removeChild(effect.getDisplayObject());
		this.effects.splice(i,1);
	}
}

robo.board.prototype.scanTilesForField = function(x, y, direction, property){
	var cardinal = {
		x: Math.cos(direction*Math.PI/2),
		y: -1*Math.sin(direction*Math.PI/2)
	};


	var scanX = x, scanY = y;

	while(scanX >= 0 && scanX <= this.width 
		&& scanY >= 0 && scanY <= this.height){
		var tile = this.getTile(scanX, scanY);
		if(tile && tile.getFieldEffects()[property]){
			return {
				x: scanX,
				y: scanY,
				found: true,
				tile: tile
			};
		}
		scanX += cardinal.x; scanY += cardinal.y;
	}

	return {
		x: scanX, 
		y: scanY, 
		tile: null,
		found: false
	};
};