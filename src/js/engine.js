window.robo = window.robo || {};
window.robo.util = window.robo.util || {};

/*****************************
		ENGINE
*****************************/

robo.engine = function(){
	this.level = null;
	this.gui = null;
	this.library = null;
	this.stage = null;
};

robo.engine.prototype.init = function(stage, regions, assets){
	this.stage = stage;
	this.gui = (new robo.gui()).init(regions);
	this.library = (new robo.library(assets)).init();

	return this;
};

robo.engine.prototype.loadLevel = function(levelJson){
	if(_.isString(levelJson)) { levelJson = JSON.parse(levelJson); }

	if(this.level){
		this.stage.removeChild(this.level.getDisplayObject());
		this.level = null;
	}

	this.level = (new robo.level()).init();
	var a = this.level.loadBoard(levelJson.map)
	var b = this.level.loadCards(levelJson.cards);
	
	this.stage.addChild(this.level.getDisplayObject());
	
	return this;
};

robo.engine.prototype.exportLevel = function(){
	if(this.level){
		return {
			map: this.level.exportBoard(),
			cards: this.level.exportCards()
		};
	}
};

robo.engine.prototype.exportLevelToString = function(){
	return JSON.stringify(this.exportLevel());
}

robo.engine.prototype.update = function(event){
	this.stage.update(event);
};
