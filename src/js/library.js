
/*****************************
		RESOURCE LIBRARY
*****************************/

robo.library = function(){
	this.collections = {};
	this.resources = [];
};

robo.library.prototype.init = function(){
	this.collections.sprite = (new robo.collection.spriteCollection()).init(this);
	this.collections.icon = (new robo.collection.iconCollection()).init(this);
	this.loader = new createjs.LoadQueue(false);
	return this;
};


robo.library.prototype.getLoader = function(){ return this.loader; };

robo.library.prototype.addResource = function(data){
	this.resources.push(data);
};

robo.library.prototype.addResources = function(arrData){
	_.each(arrData, this.addResource.bind(this));
};

robo.library.prototype.getManifest = function(){
	return _.map(this.resources, function(item){
		return {
			src: item.src,
			id: item.id
		};
	})
};

robo.library.prototype.load = function(){
	var manifest = this.getManifest();
	this.loader.loadManifest(manifest);
};	

robo.library.prototype.parseAssets = function(loader){
	var keys = _.keys(loader._loadedResults);
	var self = this;
	_.each(keys, function(key){
		var parts = key.split(".");
		if(parts.length > 1){
			var section = parts[0],
			id = parts[1];

			var options = (_.findWhere(self.resources, {id: key}) || {}).options || {};

			if(self.collections[section]){
				self.collections[section].loadResource(id, loader.getResult(key), options);
			}
		}
	})
};

robo.library.prototype.get = function(section, id){
	if(this.collections[section]){
		return this.collections[section].get(id);
	}else{
		return null;
	}
};
