window.robo = window.robo || {};
window.robo.util = window.robo.util || {};
var p;//prototype shortcut


robo.dialog = function(){
	this.init();
};

robo.dialog.prototype.init = function(){
	this.headerTitle = ko.observable("Dialog Title");
	this.dialogWidth = ko.observable("500px");
}

robo.dialog.prototype.getContainer = function(){
	return robo.dialog.$container = robo.dialog.$container || $("<div>").addClass("dialog-container").appendTo($(".main"));
}

robo.dialog.prototype.getTemplate = function(){
	if(!this.template){ return null;}
	return $("#"+this.template).html();
}

robo.dialog.prototype.open = function(){

	var $container = this.getContainer();
	this.modalOverlay = $("<div>").addClass("modal-overlay")
	var instanceBody = $("<div>")
		.addClass('dialog-instance-drag')
		.attr("data-bind", "template:{name:'dialog-instance', data: $data}")
		.appendTo(this.modalOverlay);
		// .drags({ handle: '.dialog-header'});

	this.onOpen.apply(this,arguments);

	this.modalOverlay.appendTo($container).css("top", "0")
	ko.applyBindings(this, instanceBody[0]);

	_.delay(function(){
		instanceBody.drags({handle: '.dialog-header'});
		instanceBody.css({
			top: "100px",
			left: "100px"
		});
	}, 0);
};

robo.dialog.prototype.onOpen = function(){};
robo.dialog.prototype.onClose = function(){};

robo.dialog.prototype.close = function(){
	this.onClose.apply(this, arguments);

	this.modalOverlay.remove();
};

robo.dialogs = {};

robo.dialogs.editBoardDialog = function(){
	this.init();
	this.template = 'dialog-board-edit';
};

p = robo.dialogs.editBoardDialog.prototype = Object.create(robo.dialog.prototype);
p.constructor = robo.dialogs.editBoardDialog;

p.onOpen = function(board, callback){
	console.log('hi', board);
	this.callback = callback;
	this.height = ko.observable(board.height);
	this.width = ko.observable(board.width);
	this.scale = ko.observable(board.getDisplayObject().scaleX);
};

p.save = function(){
	console.log("THIS", this);
	var data = {
		scale: this.scale(),
		width: this.width(),
		height: this.height()
	};

	if(_.isFunction(this.callback)){
		this.callback(data);
	}

	this.close();
};