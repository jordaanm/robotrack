robo.dialogs = robo.dialogs || {};

robo.dialogs.inspectDialog = function(){
	this.init();
	this.template = 'dialog-inspect';
};

p = robo.dialogs.inspectDialog.prototype = Object.create(robo.dialog.prototype);
p.constructor = robo.dialogs.inspectDialog;

p.mapSetting = function(setting){
	var obj = {
		name: setting.name,
		type: setting.type,
		subtype: setting.subtype,
		label: setting.label,		
	};

	if(setting.type == "pattern"){
		obj.value = ko.observableArray(_.map(setting.value, function(value){
			return {
				value: ko.observable(value),
				type: obj.subtype
			};
		}));
	}
	else{
		obj.value = ko.observable(setting.value);
	}

	return obj;
}

p.readSettingValue = function(setting){
	if(setting.type == "pattern"){
		return _.map(ko.unwrap(setting.value), function(item){
			return ko.unwrap(item.value);
		});
	}
	else{
		return ko.unwrap(setting.value);
	}
};

p.onOpen = function(type, item, callback){
	window.item = item;
	this.callback = callback;
	this.type = ko.observable(type);
	this.item = ko.observable(item);
	this.settings = null;
	
	if(this.type() == "tile"){
		this.settings = _.map(this.item().tileDfn.getEditSettings(this.item()), this.mapSetting);
	}

	window.settings = this.settings;
};

p.save = function(){
	var data = {};

	_.each(this.settings, function(item, key){
		data[item.name] = this.readSettingValue(item);
	}.bind(this));

	if(_.isFunction(this.callback)){
		this.callback(data);
	}

	this.close();
};