window.robo = window.robo || {};
window.robo.util = window.robo.util || {};
var p;//prototype shortcut

/***************************
   PHASES
**************************/

// Phases
robo.phase = function(){
	this.id = null;
	this.name = null;
	this.eventHandler = null;
	this.updateFn = null;
	this.activeTool = null;
};

p = robo.phase.prototype;

p.getAvailableTools = function(){ return []; };

p.initToolbar = function(){
	var bar = $('.phase-buttons').empty();

	this.tools = this.getAvailableTools();
	_.each(this.tools, function(tool){
		bar.append($("<button>")
			.addClass("btn-phase-tool")
			.html(tool.name)
			.click(function(){
				this.setTool(tool);
			}.bind(this))
		);
	}.bind(this));

};

p.setTool = function(newTool){

	if(this.activeTool){
		this.activeTool.deactivate();
	}

	this.activeTool = newTool;
	this.activeTool.activate();
};

p.onEvent = function(eventType, eventDetails){
	if(this.eventHandler){
		return this.eventHandler(eventType, eventDetails);
	}
	else{
		return false;
	}
};

p.update = function(level, timer){};

p.handleActions = function(level, timer){
	if(level.getIncompleteActions().length == 0){
		level.endRound();
		level.startRound();
	}

	var actions = level.getIncompleteActions();
	var lowestPriority = _.min(actions, 'priority').priority;

	_.each(actions, function(action){
		if(action.priority == lowestPriority){
			action.update(timer.delta/1000);
		}
	});

	// if(level.getIncompleteActions().length == 0){
	// 	level.endRound();
	// }		
};

p.connectSwitchObservers = function(level){
	level.board.connectSwitchObservers();
};

robo.phase.loadLevel = function(){
	this.id = "load";
	this.name = "Load Level";
};

p = robo.phase.loadLevel.prototype = Object.create(robo.phase.prototype);

p.eventHandler = function(eventType, eventDetails){
	if(eventType == 'startPhase'){
		//Show 1 of each tile type
		this.initToolbar();
		eventDetails.level.showEditorCards();
	}
	if(eventType == 'playCard'){

	}
	if(eventType == 'selectTile'){
		if(this.activeTool){
			this.activeTool.select('tile', eventDetails.tile, eventDetails);
		}
	}
	if(eventType == 'placeTile'){
		var x = eventDetails.tile.coord.x,
		y = eventDetails.tile.coord.y;
		eventDetails.level.board.setTile(x,y, eventDetails.tileDfn);
	}
	if(eventType == 'endPhase'){
		//Remove tile types
		eventDetails.level.hideEditorCards();
	}
};

p.getAvailableTools = function(){
	return [
		new robo.tools.inspect()
	];
};


robo.phase.playChips = function(){
	this.id = "chip";
	this.name = "Place Chips";
};

p = robo.phase.playChips.prototype = Object.create(robo.phase.prototype);

p.eventHandler = function(eventType, eventDetails){
	if(eventType == "startPhase"){
		eventDetails.level.hand.fillHand(eventDetails.level.cardDfns);
	}
	else if(eventType == "endPhase"){
		eventDetails.level.hand.cards.forEach(function(card){
			card.tapped = true;
		});				
	}
	else if(eventType == "playCard"){
		eventDetails.card.attachTo(eventDetails.tile);
	}
};

p.update = function(level, timer){
	this.handleActions(level, timer);
};

robo.phase.runSimulation = function(){
	this.id = "run";
	this.name = "Run Simulation";
};

p = robo.phase.runSimulation.prototype = Object.create(robo.phase.prototype);

p.eventHandler = function(eventType, eventDetails){
	if(eventType == "startPhase"){			
		eventDetails.level.simStarted = true;
		eventDetails.level.currentRound = eventDetails.level.round = 0;
		// eventDetails.level.startAttempt();
		this.initialTime = createjs.Ticker.getTime();

		eventDetails.level.board.tiles.forEach(function(tile){
			if(tile.tileDfn === robo.util.tileDfns.avatarStartN){
				var av = new robo.avatar();
				eventDetails.level.board.addAvatar(av);
				av.setPositionImmediate(tile.coord.x, tile.coord.y);
			}
		});

		this.connectSwitchObservers(eventDetails.level);
	}
};

p.update = function(level, timer){
	this.handleActions(level, timer);
};


/***************************
   PHASES
**************************/

	robo.util.phases = {};
	robo.util.phases.loadLevel = new robo.phase.loadLevel();
	robo.util.phases.playChips = new robo.phase.playChips();
	robo.util.phases.runSimulation = new robo.phase.runSimulation();