window.robo = window.robo || {};
window.robo.util = window.robo.util || {};
var p;//prototype shortcut
/****************************
		TILE DEFINITION
****************************/
robo.tileDfn = function(){
	this.spriteId = null;
	this.iconId = null;
	this.defaultAnimation = void 0;
	this.name = "BLANK DFN";
	return this;
};

p = robo.tileDfn.prototype;
p.buildResources = function(library){
	this.icon = library.get("icon", this.iconId);
	this.sprite = new createjs.Sprite(library.get("sprite", this.spriteId), this.defaultAnimation);
};

p.editTemplate = "edit-tile-value-template";

p.render = function(tile){
	var container = tile.getDisplayObject();
	tile.sprite = this.sprite.clone();
	container.addChild(tile.sprite);
};

p.getEffect = function(tile){ return tile.effect || false; }

p.startRound = function(args, tile){};
p.endRound = function(args, tile){};

p.update = function () {};
p.canStepOn = function() { return true; };
p.onTouch = function(avatar, tile, board){};

p.getEditSettings = function(){ return []; };
p.loadSettings = function(tile, settings){};
p.exportSettings = function(tile){return {}};

p.connectSwitchObserver = function(tile, board){};

p.getFieldEffects = function(){
	return {};
};

p.getDefaultSettings = function(){
	return {};
};
//Definitions

/*********************
        VACANT
**********************/

robo.tileDfn.vacant = function(){
	this.iconId = "tile-icon";
	this.spriteId = "tile";
	this.defaultAnimation = "idle";
	this.name = "Vacant";
};

p = robo.tileDfn.vacant.prototype = Object.create(robo.tileDfn.prototype);
p.constructor = robo.tileDfn.vacant;

/*********************
        SOLID
**********************/

robo.tileDfn.solid = function(){
	this.iconId = "tile-solid-icon";
	this.spriteId = "tile-solid";
	this.defaultAnimation = "idle";
	this.name = "Solid";
};

p = robo.tileDfn.solid.prototype = Object.create(robo.tileDfn.prototype);
p.constructor = robo.tileDfn.solid;

p.canStepOn = function(){
	return false;
};

p.getFieldEffects = function(){
	return {
		blockLaser: true
	};
};
/*********************
        PIT
**********************/

robo.tileDfn.pit = function(){
	this.iconId = "tile-pit-icon";
	this.spriteId = "tile-pit";
	this.defaultAnimation = "idle";
	this.name = "Pit";
};

p = robo.tileDfn.pit.prototype = Object.create(robo.tileDfn.prototype);
p.constructor = robo.tileDfn.pit;

p.onTouch = function(event){
	console.log("Touching Pit");
	event.level.currentAttempt.fail();
};

/*********************
        START
**********************/

robo.tileDfn.start = function(direction){
	this.iconId = "tile-start-icon";
	this.spriteId = "tile-start";
	this.defaultAnimation = "idle";
	this.direction = direction;
	this.name = "Start";
};

p = robo.tileDfn.start.prototype = Object.create(robo.tileDfn.prototype);
p.constructor = robo.tileDfn.start;

p.onTouch = function(event){
	console.log("Touching Avatar Start");
};


p.loadSettings = function(tile, settings){
	tile.state.direction = settings.direction;
};

p.exportSettings = function(tile){
	return {
		direction: tile.state.direction
	};
};


p.getEditSettings = function(tile){ 
	return [
		{
			name: 'direction',
			label: 'Facing Direction',
			value: tile.state.direction,
			type: 'direction'
		}
	];
};

/*********************
        END
**********************/

robo.tileDfn.end = function(){
	this.iconId = "tile-end-icon";
	this.spriteId = "tile-end";
	this.defaultAnimation = "idle";
	this.name = "End";
};

p = robo.tileDfn.end.prototype = Object.create(robo.tileDfn.prototype);
p.constructor = robo.tileDfn.end;

p.onTouch = function(event){
	console.log("Touching Avatar End", event);
	event.level.currentAttempt.win();
};

/*********************
        DOOR
**********************/

robo.tileDfn.door = function(){
	this.iconId = "tile-door-icon";
	this.spriteId = "tile-door";
	this.defaultAnimation = "opened";
	this.name = "Door";
};

p = robo.tileDfn.door.prototype = Object.create(robo.tileDfn.prototype);
p.constructor = robo.tileDfn.door;

p.canStepOn = function(tile){
	return !!tile.state.open; 
};

p.getFieldEffects = function(tile){
	return {
		blockLaser: !tile.state.open
	};
};

p.startRound = function(args, tile){
	var prev = tile.state.open;
	tile.state.open = tile.state.openBehaviour.getStateForRound(args.round);

	var animName = '';
	if(tile.state.open){ animName = prev? 'open' : 'opening'; }
	else{ animName = prev? 'closing': 'closed'; }

	var action = new robo.roundAction.animateSprite(tile.sprite, animName);
	action.setPriority(tile.state.open? 7 : 3);
	args.actions.push(action);
};

p.getDefaultSettings = function(){
	return {
		startOpen: true,
		openPattern: [true, false],
		openBehaviourType: 'pattern'
	};
}

p.loadSettings = function(tile, settings){
	var fullSettings = $.extend(true, {}, this.getDefaultSettings(), settings);
	tile.state.startOpen = tile.state.open = fullSettings.open;
	tile.state.pattern = fullSettings.openPattern;
	tile.state.openBehaviourType = fullSettings.openBehaviourType;
	tile.state.openBehaviour = new robo.roundBehaviour[fullSettings.openBehaviourType](tile.state, 'pattern');	
};

p.exportSettings = function(tile){
	return {
		open: tile.state.startOpen,
		openBehaviourType: tile.state.openBehaviourType,
		openPattern: tile.state.pattern
	};
};

p.connectSwitchObserver = function(tile, board){
	if(tile.state.openBehaviour){
		tile.state.openBehaviour.connectSwitchObserver(board.eventer);
	}
};

p.getEditSettings = function(tile){ 
	return [
		{
			name: 'startOpen',
			label: 'Start Open',
			value: tile.state.startOpen,
			type: 'bool'
		},
		{
			name: 'pattern',
			label: 'Open/Close Pattern',
			value: tile.state.openPattern,
			type: 'pattern',
			subtype: 'bool'
		},
		{
			name: 'openBehaviourType',
			label: 'Behaviour Type',
			value: tile.state.openBehaviourType,
			type: 'behaviour'
		}
	];
};


/*********************
        LASER
**********************/

robo.tileDfn.laser = function(){
	this.iconId = "tile-laser-icon";
	this.spriteId = "tile-laser";
	this.defaultAnimation = "activating";
	this.name = "Laser";
};

p = robo.tileDfn.laser.prototype = Object.create(robo.tileDfn.prototype);
p.constructor = robo.tileDfn.laser;

p.canStepOn = function(tile){
	return true;
};

p.createEffect = function(board, tile){
	var effect = new robo.fieldEffect.beam(board, tile);
	board.addEffect(effect);
	return effect;
}

p.startRound = function(args, tile){
	var prev = tile.state.active;
	if(!tile.effect){
		tile.effect = this.createEffect(args.board, tile);
	}

	tile.state.active = this.getOpenBehaviour(tile).getStateForRound(args.round);
	tile.state.direction = tile.state.directionBehaviour.getStateForRound(args.round);

	var action = new robo.roundAction.animateSprite(tile.sprite, tile.state.active ? 'activating': 'deactivating');
	action.setPriority(tile.state.open? 7 : 3);
	args.actions.push(action);

	tile.effect.setAttr({ active: tile.state.active, direction: tile.state.direction});
	var laserAction = new robo.roundAction.beam(tile.effect);
	args.actions.push(laserAction);
};

p.getOpenBehaviour = function(tile){
	return tile.state.openBehaviour  = tile.state.openBehaviour || new robo.roundBehaviour.alternate(tile.state, 'active'); 
}

p.getDefaultSettings = function(){
	return {
		directionPattern: [0,1,2,3],
		direction: 0,
		directionBehaviourType: 'pattern',
		active: false,
		openPattern: [true, false],
		openBehaviourType: 'pattern',
		openChannel: 0
	};
};

p.loadSettings = function(tile, settings){
	settings = $.extend(true, {}, this.getDefaultSettings(), settings || {});

	tile.state.initialDirection = tile.state.direction = settings.direction;
	tile.state.directionPattern = settings.directionPattern;
	tile.state.directionBehaviourType = settings.directionBehaviourType
	tile.state.directionBehaviour = new robo.roundBehaviour[settings.directionBehaviourType](tile.state, 'directionPattern');
	
	tile.state.startActive = tile.state.active = settings.active;	
	tile.state.openPattern = settings.openPattern;
	tile.state.openBehaviourType = settings.openBehaviourType;
	tile.state.openChannel = settings.openChannel;

	var behaviourKey = settings.openBehaviourType == "switch" ? 'active' : 'openPattern';
	tile.state.openBehaviour = new robo.roundBehaviour[settings.openBehaviourType](tile.state, behaviourKey, tile.state.openChannel);
};

p.exportSettings = function(tile){
	return {
		direction: tile.state.initialDiraction,
		directionPattern: tile.state.directionPattern,
		directionBehaviourType: tile.state.directionBehaviourType,

		active: tile.state.startActive,
		openPattern: tile.state.openPattern,
		openBehaviourType: tile.state.openBehaviourType,
		openChannel: tile.state.openChannel
	};
};

p.getFieldEffects = function(tile){
	return {
		blockLaser: !tile.state.active
	};
};


p.getEditSettings = function(tile){ 
	return [
		{
			name: 'direction',
			label: 'Initial Direction',
			value: tile.state.initialDirection,
			type: 'direction'
		},
		{
			name: 'directionPattern',
			label: 'Direction Pattern',
			value: tile.state.directionPattern,
			type: 'pattern',
			subtype: 'direction'
		},
		{
			name: 'directionBehaviourType',
			label: 'Direction Behaviour Type',
			value: tile.state.directionBehaviourType,
			type: 'behaviour'
		},
		{
			name: 'startActive',
			label: 'Start Active',
			value: tile.state.startActive,
			type: 'bool'
		},
		{
			name: 'openBehaviourType',
			label: 'Behaviour Type',
			value: tile.state.openBehaviourType,
			type: 'behaviour'
		},
		{
			name: 'openPattern',
			label: 'Open/Close Pattern',
			value: tile.state.openPattern,
			type: 'pattern',
			subtype: 'bool'
		},
		{
			name: 'openChannel',
			label: 'Open/Close Switch Channel',
			value: tile.state.openChannel,
			type: 'channel'
		}
	];
};

p.connectSwitchObserver = function(tile, board){
	if(tile.state.openBehaviour){
		tile.state.openBehaviour.connectSwitchObserver(board.eventer);
	}
};

/*********************
        SWITCH
**********************/

robo.tileDfn.switch = function(){
	this.iconId = "tile-switch-icon";
	this.spriteId = "tile-switch";
	this.defaultAnimation = "on";
	this.name = "Switch";
};

p = robo.tileDfn.switch.prototype = Object.create(robo.tileDfn.prototype);
p.constructor = robo.tileDfn.switch;

p.canStepOn = function(tile){
	return true;
};

p.startRound = function(args, tile){};

p.getDefaultSettings = function(){
	return {
		active: false,
		channel: 0
	};
};

p.loadSettings = function(tile, settings){
	settings = $.extend(true, {}, this.getDefaultSettings(), settings);
	tile.state.startActive = tile.state.active = settings.active;
	tile.state.channel = settings.channel || 0;
};

p.exportSettings = function(tile){
	return {
		active: tile.state.startActive,
		channel: tile.state.channel,
	};
};

p.getEditSettings = function(tile){ 
	return [
		{
			name: 'startActive',
			label: 'Start Active',
			value: tile.state.startActive,
			type: 'bool'
		},
		{
			name: 'channel',
			label: 'Channel',
			value: tile.state.channel,
			type: 'channel'
		},
	];
};

p.onTouch = function(event){
	console.log("Activating Switch");
	event.board.eventer.trigger('switch', {
		tile: event.tile, 
		on: event.tile.state.active, 
		channel: event.tile.state.channel
	});
};