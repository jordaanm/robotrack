robo.util = robo.util || {};

robo.util.readAttribute = function(target, propertyChain){
	target = ko.utils.unwrapObservable(target);

	if (target == null || target == "undefined") {
		return target;
	}

	// Check if the property is as-specified
	if (target[propertyChain[0]]) {
		target = target[propertyChain[0]];
	}
	else {
		target = undefined;
	}

	propertyChain = propertyChain.slice(1);

	if (propertyChain.length > 0) {
		return robo.util.readAttribute(target, propertyChain);
	}
	else {
		return target;
	}	
}

robo.util.dotRead = function(target, dotNotation){
	try {
		return ko.unwrap(robo.util.readAttribute(target, dotNotation.split(".")));
	}
	catch(ex){
		console.trace();
		throw new Error(ex);
	}
}

robo.util.toggleObs = function(obs){
	obs(!obs());
};

robo.util.setObs = function(obs, value){
	return function(){
		obs(value);
	};
};

robo.util.pattern = {};

robo.util.pattern.add = function(obs, type, defaultValue){
	return function(){
		obs.push({
			type: type,
			value: ko.observable(defaultValue)
		});
	};
};

robo.util.pattern.remove = function(obs){
	return function(){
		if(obs().length > 1){				
			obs.pop();
		}
	};
};