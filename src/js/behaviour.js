window.robo = window.robo || {};
window.robo.util = window.robo.util || {};
var p;//prototype shortcut


robo.roundBehaviour = function(){

};

robo.roundBehaviour.prototype.getStateForRound = function(round){
	return this.obj[key];
};

robo.roundBehaviour.prototype.set = function(object, key){
	this.obj = object;
	this.key = key;
	return this;
};

robo.roundBehaviour.prototype.connectSwitchObserver = function(eventer){
};

robo.roundBehaviour.alternate = function(object, key){
	this.set(object, key);
};
p = robo.roundBehaviour.alternate.prototype = Object.create(robo.roundBehaviour.prototype);
p.constructor = robo.roundBehaviour.alternate;

p.getStateForRound = function(round){
	return round%2 ? this.obj[this.key] : !this.obj[this.key];
};

robo.roundBehaviour.pattern = function(object, key){
	this.set(object, key);
};
p = robo.roundBehaviour.pattern.prototype = Object.create(robo.roundBehaviour.prototype);
p.constructor = robo.roundBehaviour.pattern;

p.getStateForRound = function(round){
	var l = this.obj[this.key].length;
	var val = this.obj[this.key][round%l];
	return val;
};


robo.roundBehaviour.switch = function(object, key, channel){
	this.set(object, key);
	this.channel = channel || 0;
};

p = robo.roundBehaviour.switch.prototype = Object.create(robo.roundBehaviour.prototype);
p.constructor = robo.roundBehaviour.switch;

p.getStateForRound = function(round){
	return this.obj[this.key];
};

p.connectSwitchObserver = function(eventer){
	eventer.subscribe('switch', function(data){
		if(data.channel == this.channel){
			this.obj[this.key] = !this.obj[this.key];
		}
	}.bind(this), {}, this);
};
	//Behaviour = switch
	//NOTHING, open/close using connectSwitch

