window.robo = window.robo || {};
window.robo.util = window.robo.util || {};

var p; // prototype shortcut
/****************************
		ASSET COLLECTIONS
****************************/

robo.collection = function(){
	this.items = {};
	this.name = "";
	this.library = null;
	this.resources = [];
};

robo.collection.prototype.init = function(library){
	this.library = library;
	return this;
};

robo.collection.prototype.add  =function(name, value){
	this.items[name] = value;
};

robo.collection.prototype.remove = function(name){
	delete this.items[name];
};

robo.collection.prototype.get = function(name){
	return this.items[name];
}

robo.collection.prototype.getResources = function(){

};

robo.collection.prototype.parse = function(data){

};

robo.collection.prototype.loadResource = function(id, data, settings){
	var converted = this.parse(data, settings);
	this.items[id] = converted;
}

robo.collection.spriteCollection = function(){
	this.name = "sprite";
};

robo.collection.spriteCollection.prototype = new robo.collection();
robo.collection.spriteCollection.prototype.constructor = robo.collection.spriteCollection;
p = robo.collection.spriteCollection.prototype;

p.parse = function(data, settings){
	var obj = $.extend(true, 
		{
			images: [data]
		}, 
		{
			"frames": {
				regX: 0,
				regY: 0,
				height: 50,
				width: 50,
				count: 1
			},
			"animations": {"idle": [0,0,"idle", 1.0]}
		},
		settings
	);

	return new createjs.SpriteSheet(obj);
};

robo.collection.iconCollection = function(){
	this.name = "icon";
};
robo.collection.iconCollection.prototype = new robo.collection();
robo.collection.iconCollection.prototype.constructor = robo.collection.iconCollection;
p = robo.collection.iconCollection.prototype;

p.parse = function(data, settings){
	return new createjs.Bitmap(data)
};