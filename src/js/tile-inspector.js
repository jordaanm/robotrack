window.robo = window.robo || {};
window.robo.util = window.robo.util || {};
var p;//prototype shortcut

robo.tileInspector = function() {
	this.initialize();
}

p = robo.tileInspector.prototype = new createjs.Container();

p.Container_initialize = p.initialize;
p.initialize = function() {
	this.Container_initialize();
	this.iconId = 'tile-inspect-icon';
	this.icon = null;
	// add custom setup logic here.
}

p.setIcon = function(library){
	if(this.icon){
		this.removeChild(this.icon);
	}
	this.icon = library.get("icon", this.iconId).clone();
	this.addChild(this.icon);
}