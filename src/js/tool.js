// tool
	//eventer -> on change, on use, etc

// level edit tool
	//Inspector
	//StampTile

// place chips tool

	//Inspector
	//StampChip

window.robo = window.robo || {};
window.robo.util = window.robo.util || {};
var p;//prototype shortcut

robo.tool = function(){
	this.active = false;
	this.name = "No Tool";
	this.bleep = 'bloop';
}

p = robo.tool.prototype;

p.activate = function(){
	this.active = true;
};
p.deactivate = function(){
	this.active = true;
};

p.select = function(type, item, options){};


robo.tools = {};

robo.tools.inspect = function(){
	this.active = false;
	this.name = "Inspect";
};

p = robo.tools.inspect.prototype = Object.create(robo.tool.prototype);

p.select = function(type, item, options){
	var dialog = new robo.dialogs.inspectDialog();
	dialog.open(type, item, function(settings){
		if(type == "tile"){
			console.log("Applying settings to tile");
			item.tileDfn.loadSettings(item, settings);
		}
	});
};