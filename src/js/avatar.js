window.robo = window.robo || {};
window.robo.util = window.robo.util || {};

/****************************
		ROBOT AVATAR
****************************/

robo.avatar = function(){
	var self = this;
	self.displayObject = null;
	self.coord = {x: 0, y: 0};
	self.state = {};
	self.facing = 0;
	self.setFacing(0);
	self.isDead = false;
	return self;
};

robo.avatar.prototype.setBoard = function(board){
	this.board = board;
};

robo.avatar.prototype.buildDisplayObject = function(state){

	var crono = new createjs.Sprite(robo.util.sprites.crono, "walkRight");
	crono.framerate = 4;
	crono.name = "CronoBot";
	crono.scaleX = crono.scaleY = 0.625;

	var dObj = new createjs.Container();
	dObj.addChild(crono);
	dObj.name = "Avatar";

	return dObj;
};

robo.avatar.prototype.facingMap = {
	0: 'walkRight',
	1: 'walkUp',
	2: 'walkLeft',
	3: 'walkDown',
};

robo.avatar.prototype.facingDisplacement = {
	0: {x: 1, y: 0},
	1: {x: 0, y: -1},
	2: {x: -1, y: 0},
	3: {x: 0, y: 1}
};

robo.avatar.prototype.turnCW = function(turns){
	this.setFacing((this.facing - turns) % 4);
}

robo.avatar.prototype.turnCCW = function(turns){
	this.setFacing((this.facing + turns) % 4);
}

robo.avatar.prototype.getDisplayObject = function(){
	var self = this;
	if(self.displayObject == null){
		self.displayObject = self.buildDisplayObject();
	};
	return (self.displayObject);
};

robo.avatar.prototype.setFacing = function(newFacing){
	//Normalise to 0-4
	while(newFacing < 0){
		newFacing += 4;
	}

	var self = this;
	self.facing = newFacing;
	self.getDisplayObject().children[0].gotoAndPlay(self.facingMap[newFacing]);

};


robo.avatar.prototype.canMoveTo = function(level, x, y){
	// return true;
	var tile = level.board.getTile(x, y);
	return tile && tile.canStepOn();
}

robo.avatar.prototype.tryWalkForward = function(level){
	 var displacement = this.facingDisplacement[this.facing];
	 var x = this.coord.x + displacement.x;
	 var y = this.coord.y + displacement.y;

	if(this.canMoveTo(level, x, y)){
		this.walkForward();
	}
};

robo.avatar.prototype.walkForward = function(){
	var displacement = this.facingDisplacement[this.facing];
	this.coord.x += displacement.x;
	this.coord.y += displacement.y;
};

robo.avatar.prototype.setPositionImmediate = function(x, y){
	var pos = this.board.getTileCenter(x,y);
	var bounds = this.getDisplayObject().getBounds();
	this.getDisplayObject().x = pos.x - bounds.width/2;
	this.getDisplayObject().y = pos.y - bounds.height/2;
	this.coord.x = x; this.coord.y = y;
};

robo.avatar.prototype.startRound = function(args){
	// console.log("ROBO START ROUND", args);
	if(this.isDead){
		args.level.currentAttempt.fail();
	}
	
	this.tryWalkForward(args.level);
	var bounds = this.board.getCoordBounds(this.coord.x, this.coord.y);

	var action = new robo.roundAction.animatePosition(
		this.getDisplayObject(), 
		{x: bounds.left, y: bounds.top}, 
		2.0
	);

	action.setPriority(5);

	args.actions.push(action);
};

robo.avatar.prototype.endRound = function(args){
	this.action = null;
	// console.log("ROBO END ROUND", args);
};

robo.avatar.prototype.update = function(event){

};

robo.avatar.prototype.die = function(){
	this.isDead = true;
};